import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxGalleryModule } from 'ngx-gallery';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { TabsModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SampleOneComponent } from './products/sample-one/sample-one.component';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { ProductDetailsComponent } from './products/sample-one/product-details/product-details.component';
import { SaeshinProductDetailsComponent } from './products/Sample-one/saeshin-product-details/saeshin-product-details.component';
import { CarloProductDetailsComponent } from './products/Sample-one/carlo-product-details/carlo-product-details.component';
import { HandPiecesHandInstrumentsComponent } from './products/hand-pieces-hand-instruments/hand-pieces-hand-instruments.component';
import { SocoComponent } from './products/hand-pieces-hand-instruments/soco/soco.component';
import { CrownAndBridgeComponent } from './products/crown-and-bridge/crown-and-bridge.component';
import { BursComponent } from './products/burs/burs.component';
import { DisposablesComponent } from './products/disposables/disposables.component';
import { PrimeDentalComponent } from './products/crown-and-bridge/prime-dental/prime-dental.component';
import { MaquiraComponent } from './products/disposables/maquira/maquira.component';
import { LaridentComponent } from './products/disposables/larident/larident.component';
import { MedProtectComponent } from './products/disposables/med-protect/med-protect.component';
import { PromiseeComponent } from './products/disposables/promisee/promisee.component';
import { WrRayComponent } from './products/disposables/wr-ray/wr-ray.component';

const appRoutes: Routes = [
  { path: 'HomePage', component:HomepageComponent },
  {path:'ContactUs', component:ContactUsComponent},
  //.................Equipment page routing and its products..........
  {path:'Products/Equipments',component:SampleOneComponent },
  {path:'Products/Equipments/SUN/:type', component:ProductDetailsComponent},
  {path:'Products/Equipments/Saeshin/:type', component:SaeshinProductDetailsComponent},
  {path:'Products/Equipments/Carlo de giorgi/:type', component:CarloProductDetailsComponent},
  //..................HandPieces and Instruments page routing and its products.........
  {path:'Products/HandPieces and HandInstruments',component:HandPiecesHandInstrumentsComponent},
  {path:'Products/HandPieces and HandInstruments/SOCO/:type', component:SocoComponent},
  //......................Crown and Bridges page routing................................................
  {path:'Products/CrownAndBridges',component:CrownAndBridgeComponent},
  {path:'Products/CrownAndBridges/product/:type', component:PrimeDentalComponent},
  //............................BURS pages Routing................................................................
  {path:'Products/Burs',component:BursComponent},
  //.............................Disposables page Routing..............................................
  {path:'Products/Disposables',component:DisposablesComponent},
  {path:'Products/Disposables/Maquira/:type', component:MaquiraComponent},
  {path:'Products/Disposables/Larident/:type', component:LaridentComponent},
  {path:'Products/Disposables/MedProtect/:type', component:MedProtectComponent},
  {path:'Products/Disposables/Citisen/:type', component:PromiseeComponent},
  {path:'Products/Disposables/W.R.Rayson/:type', component:WrRayComponent},
  { path: '',redirectTo: '/HomePage',pathMatch: 'full'},
 
];

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    ContactUsComponent,
    SampleOneComponent,
    ProductDetailsComponent,
    SaeshinProductDetailsComponent,
    CarloProductDetailsComponent,
    HandPiecesHandInstrumentsComponent,
    SocoComponent,
    CrownAndBridgeComponent,
    BursComponent,
    DisposablesComponent,
    PrimeDentalComponent,
    MaquiraComponent,
    LaridentComponent,
    MedProtectComponent,
    PromiseeComponent,
    WrRayComponent
    
  ],
  imports: [
    BrowserModule,
    NgxGalleryModule,
    FilterPipeModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyCfTuB2SDEMdQvMj5Ndt_Q5skeDfH2c9UI'
    }),
    TabsModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
