import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SendMsg } from '../forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  send_form:FormGroup;
  sendMsg:SendMsg[];
  lat: number = 29.983919;
  lng: number = 31.314636;
    constructor(private fb:FormBuilder) { }
  
    ngOnInit() {
      window.scrollTo(0, 0)
      this.send_form=this.fb.group({
        firstName:['',Validators],
        lastName:['',Validators],
        email:['',Validators],
        message:['',Validators]
      })
    }

}
