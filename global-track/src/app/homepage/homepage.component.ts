import { Component, OnInit, AfterContentInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryImageSize } from 'ngx-gallery';
import * as $ from 'jquery';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
    //about_header:string='Dr.Mohammed abd Elmawaly';
    about_para1:string='Global Trade is a specialized dental supply company that serves dentists across Egypt with the highest quality dental products.';
    about_para2:string='The company was established in 2009 by Dr. Mohamed Abdelbary Nada who comes from a dental background. Dr. Mohamed was graduated from the Faculty of Oral & Dental Medicine - Cairo University and worked as a dentist for more than ten years before moving to the sales side of the dental industry.';
    about_para3:string='In 1993, he formed his first company Dento Medical which was the first company to introduce the well-known Dentsply Company to the Egyptian market. Building upon the success of his company he opened two other companies Medical Service and Star Group. And after 16 years of strong international business relationships Dr. Mohamed established Global Trade which has expanded over time to become one of the top market leaders in Egypt.';
    about_para4:string='Global Trade has distribution agreement deals with the world’s largest manufacturers of professional dental solutions to offer a complete diverse range of dental products to the market. Our company deals with Carlo De Giorgi, Coxo-Soco, Larident, Prime Dental, Saeshin and other companies from around the globe to supply the market with a full line of dental products ranging from all disposable products to hand pieces, burs and stones, hand instruments, restorative materials and autoclaves.';
    about_para5:string='Our company has established a solid reputation in the Egyptian market over 25 years of work experience in the dental industry. Such reputation is attributed to our competency in supplying the market with the right product-market fit, delivering quality products at competitive prices, and communicating effectively with our distributors and end-users.';
    about_para6:string='Our focused approach and eagerness to replenish the market with the latest products is our motto!';
    lat: number = 29.983919;
    lng: number = 31.314636;
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
  constructor() { }

  ngOnInit() : void {
    window.scrollTo(0, 0);
    this.galleryOptions = [
        {
            width: '100%',
            height: '485px',
            preview: false,
            imageAutoPlay:true,
            imageAutoPlayPauseOnHover:true,
            thumbnails:false,
            imageArrowsAutoHide:false,
            imageInfinityMove:true,
            imageAnimation: NgxGalleryAnimation.Slide,
            imageSize:NgxGalleryImageSize.Cover
        },
        {
            breakpoint: 1200,
           imageSize:NgxGalleryImageSize.Contain
        },
        // max-width 800
        {
            breakpoint: 800,
            width: '100%',
            height: '330px',
            imagePercent: 80,
           
        },
        // max-width 400
        {
            breakpoint: 400
            
        }
    ];

    this.galleryImages = [
        {
            medium: '../../assets/imgs/a22.png',
        },
        {
            medium: '../../assets/imgs/WhatsApp Image 2018-08-01 at 5.02.35 PM.jpeg',
        },
        {
            medium: '../../assets/imgs/WhatsApp Image 2018-07-30 at 9.31.17 PM.jpg',
        },
        {
            medium: '../../assets/imgs/Untitled-2.jpg',
        },
        {
            medium: '../../assets/imgs/Untitled-311.jpg',
        },
        {
            medium: '../../assets/imgs/SUN.jpg',
        },
        {
            medium: '../../assets/imgs/20141227034251073836.jpg',
        }
    ];
}
}
