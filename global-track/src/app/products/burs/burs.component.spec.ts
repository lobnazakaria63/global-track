import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BursComponent } from './burs.component';

describe('BursComponent', () => {
  let component: BursComponent;
  let fixture: ComponentFixture<BursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
