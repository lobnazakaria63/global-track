import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Products} from "../../../models/products";
@Component({
  selector: 'app-burs',
  templateUrl: './burs.component.html',
  styleUrls: ['./burs.component.css']
})
export class BursComponent implements OnInit {
  products:Products[]=[
    {
      image:"toboom/composite polishing kit",
      key:"pToboom",
      name:'composite polishing kit',
      company:"Toboom"
    },
    {
      image:"toboom/kit of inlay and onlay preparation",
      key:"pToboom",
      name:'kit of inlay and onlay preparation',
      company:"Toboom"
    },
    {
      image:"toboom/kit of preparation teeth for porcelain veneers",
      key:"pToboom",
      name:'kit of preparation teeth for porcelain veneers',
      company:"Toboom"
    },
    {
      image:"toboom/kit preparation anterior and posterior teeth for ceramics and zirconia",
      key:"pToboom",
      name:'kit preparation anterior and posterior teeth for ceramics and zirconia',
      company:"Toboom"
    },
    {
      image:"toboom/polishers for amalgam and non-precious alloys",
      key:"pToboom",
      name:'polishers for amalgam and non-precious alloys',
      company:"Toboom"
    },
    {
      image:"toboom/polishing kit for porcelain",
      key:"pToboom",
      name:'polishing kit for porcelain',
      company:"Toboom"
    },
    {
      image:"PMD/WhatsApp Image 2018-07-30 at 9.31.17 PM",
      key:"pPMD",
      name:'',
      company:"PMD"
    },
  ];
  filter1: any={key:'Toboom'};
  filter2:any={key:'PMD'};
  filterAll:any={key:'p'};
  filter=this.filterAll;
  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  filterAllFunc(){
    this.filter=this.filterAll;
  }
  filterOneFunc(){
    this.filter=this.filter1;
  }
  filterTwoFunc(){
    this.filter=this.filter2;
  }
}
