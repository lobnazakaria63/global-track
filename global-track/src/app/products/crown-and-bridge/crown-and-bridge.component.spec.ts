import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrownAndBridgeComponent } from './crown-and-bridge.component';

describe('CrownAndBridgeComponent', () => {
  let component: CrownAndBridgeComponent;
  let fixture: ComponentFixture<CrownAndBridgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrownAndBridgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrownAndBridgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
