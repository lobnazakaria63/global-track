import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Products} from "../../../models/products";
@Component({
  selector: 'app-crown-and-bridge',
  templateUrl: './crown-and-bridge.component.html',
  styleUrls: ['./crown-and-bridge.component.css']
})
export class CrownAndBridgeComponent implements OnInit {
  products:Products[]=[
    {
      image:"prime dental/1_4_70",
      key:"pPrime",
      name:'Porcelain Etchant Gel',
      company:"Prime Dental"
    },
    {
      image:"prime dental/310Nnpg4vZL",
      key:"pPrime",
      name:'PRIME-GEL',
      company:"Prime Dental"
    },
    {
      image:"prime dental/f80b3dd9aad54d2d2e4621072ac98fac",
      key:"pPrime",
      name:'Dual Cure Composite Luting Cement',
      company:"Prime Dental"
    },
    {
      image:"prime dental/Hybrid Composite Kit",
      key:"pPrime",
      name:'Hybrid Composite',
      company:"Prime Dental"
    },
    {
      image:"prime dental/Prime Core DC Automix Syringe Core Build Up Material (Prime Dental)",
      key:"pPrime",
      name:'Prime Core DC',
      company:"Prime Dental"
    },
    {
      image:"prime dental/prime-crown-prime-dent (1)",
      key:"pPrime",
      name:'Prime Crown',
      company:"Prime Dental"
    },
    {
      image:"prime dental/prime-dent-flowable-004-010A1",
      key:"pPrime",
      name:'Flowable Composite',
      company:"Prime Dental"
    },
    {
      image:"prime dental/s-l1600",
      key:"pPrime",
      name:'Porcelain Bond Enhancer',
      company:"Prime Dental"
    },
    {
      image:"densell/Endo_Densell_4c1648522d21d",
      key:"pDensell",
      name:'Endo-G-Fill',
      company:"Densill"
    },
    {
      image:"ristea/Glass Ionomer Cement",
      key:"pRistea",
      name:'Glass Ionomer Cement',
      company:"Ristea"
    },
  ];
  filter1: any={key:'Prime'};
  filter2:any={key:'Densell'};
  filter3:any={key:'Ristea'};
  filterAll:any={key:'p'};
  filter=this.filterAll;
  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  filterAllFunc(){
    this.filter=this.filterAll;
  }
  filterOneFunc(){
    this.filter=this.filter1;
  }
  filterTwoFunc(){
    this.filter=this.filter2;
  }
  filterThreeFunc(){
    this.filter=this.filter3;
  }
}
