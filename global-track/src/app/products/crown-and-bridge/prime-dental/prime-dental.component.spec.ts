import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeDentalComponent } from './prime-dental.component';

describe('PrimeDentalComponent', () => {
  let component: PrimeDentalComponent;
  let fixture: ComponentFixture<PrimeDentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeDentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeDentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
