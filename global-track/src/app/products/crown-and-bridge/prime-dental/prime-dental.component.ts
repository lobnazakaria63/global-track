import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductDesc, PrimeProductBenefitAndFeature, PrimeProductTechData, RisteaProductInfo } from '../../../../models/crownAndBridgeProductDet';

@Component({
  selector: 'app-prime-dental',
  templateUrl: './prime-dental.component.html',
  styleUrls: ['./prime-dental.component.css']
})
export class PrimeDentalComponent implements OnInit {
  productImg:string;
  productTitle:string;
  tableHeader:string[];
  isBenefit=false;
  isTech=false;
  isTable=false;
  isTechText=false;
  isHr=true;
  productDesc:ProductDesc[];
  benefitAndFeature:PrimeProductBenefitAndFeature[];
  techData:PrimeProductTechData[];
  risteaInfo:RisteaProductInfo[];
  techDataText:ProductDesc;
  //.........................Prime Denta Products ....................
  /*......................PORCELAIN ETCHANT GELL .........................*/
  primePorcelainGellDesc:ProductDesc[]=[
    {text:"10% hydrofluoric acid gel conditions porcelain or ceramic surfaces prior to bonding. Used prior to Prime-Dent© porcelain bond enhancer."}
  ];
  /*......................PRIME GELL .........................*/
  primeGelDesc:ProductDesc[]=[
    {text:"Fast acting and effective formula of 20% benzocain. Formulated using superior quality flavorings."},
    {text:"Prime-Gel™ features pleasant tasting during usage and leaves no bitter afterTaste."}
  ];
  /*......................Dual Cure Composite Luting Cement .........................*/ 
  dualCureCementDesc:ProductDesc[]=[
    {text:"The cement is for use in final cementations of implant prosthesis, crown and bridges, inlays, onlays, veneers, cementation of posts & pins and periodontal splinting."}
  ];
  dualCureCementBenefits:PrimeProductBenefitAndFeature[]=[
    {point:"Quick Cementation"},
    {point:"Automix Handheld Syringe"},
    {point:"Convenient, economical and especially useful for mulit-operatory practices"},
    {point:"No mixing, no air bubbles, no wasted material due to over mixing."},
    {point:"Ensures, predictable, accurate, consistent and homogeneous mix every time."},
    {point:"Minimal heat generated"},
    {point:"Protects pulps"},
    {point:" Radiopaque"},
    {point:"Easy identification on x-rays"},
    {point:"Low film thickness"},
    {point:"Provides easy placement of crowns and bridges"},
    {point:"Fluoride release"},
    {point:"Reduces secondary caries"},
  ];
  dualCureCementTechDataText:ProductDesc={
    text:"Composition Glass Ionomer in a Bis-GMA based matrix of dental resins, activator, catalyst and pigments."
  };
  dualCureCementTechData:PrimeProductTechData[]=[
    {variable:"Working time",value:" 2 minutes"},
    {variable:"Setting time",value:"5 minutes"},
    {variable:"Compressive strength",value:"266 MPa"},
    {variable:"Flexural strength",value:"133 MPa"},
  ];
  /*......................Hybrid Composite .........................*/
  hybridDesc:ProductDesc[]=[
    {text:"A light cured resin-based composite used for all types of cavity preparations. An ideal choice for both anterior and posterior applications."},
    {text:"Easy to mold, sculpt and polish to a high luster."},
    {text:"Has high compressive strength as well as low shrinkage and resists wear and staining."},
    {text:"Radiopaque for easy identifications in radiographs. Incisal (translucent) and dentin (opaque) shades available."},
  ];
  hybridBenefits:PrimeProductBenefitAndFeature[]=[
    {point:"Available in 15 Vita shades"},
    {point:"High viscosity non-sticky formula for optimum handling"},
    {point:"Hybrid formula provides strength and polishability"},
    {point:"Fast, deep light activated in only 20 seconds for lighter shades"},
    {point:"Radiopaque"},
  ];
  /*......................Prime Core DC .........................*/
  primeCoreDesc:ProductDesc[]=[
    {text:"A composite based core material designed for the easy fabrication of core build-ups. Auto mixed within mixing cannulas providing a consistent, homogeneous mix. Intra oral application tips are provided for precise, direct placement of core material."},
    {text:"Dispensed with an applicator gun and auto mixed in mixing cannulas providing a consistent, homogeneous mix"},
    {text:"Intro oral application tips are provided for precise, direct placement of core material"},
    {text:"Available in a contrast blue shade which is easily distinguishable from tooth structure and a natural (A2) shade for optimum results beneath ceramic crowns."},
    {text:"The core material releases fluoride to help reduce secondary caries."},
    {text:"Dual curing and quick setting eliminating time consuming layering of material and allowing for easy bulk fill."},
    {text:"Highly radiopaque and exhibits a short setting time without high heat generation."},
  ];
  /*......................Prime Crown .........................*/
  crownDesc:ProductDesc[]=[
    {text:"A self cured bis-acryl auto-mix system with low viscosity, composed of Ethoxylated, Bis-gma multifunctional methacrylate esters, glass filler and nanofiller for a simple alternative to traditional materials."},
    {text:"Prime-Crown™ produces minimal odor, shrinkage and heat generation. Good wear resistance for better translucency."},
    {text:" Obtaining a smooth surface for esthetic appearance and protection against staining with a satisfactory working time."},
  ];
  crownBenefits:PrimeProductBenefitAndFeature[]=[
    {point:"High polish"},
    {point:"High compressive strength"},
    {point:"Six popular vita shades: A1, A2, A3, A3.5, B1, B2"},
    {point:"Low shrinkage"},
    {point:"Low heat generation"},
  ];
  crownTechData:PrimeProductTechData[]=[
    {variable:"Compressing strength", value:"340 MPa"},
    {variable:"Transverse strength", value:"76 MPa"},
    {variable:"Vickers hardness", value:"25 MPa"},
    {variable:"Linear polymerization shrinkage", value:"18%"},
    {variable:"Radiopaque", value:"Yes"},
  ];
  /*......................Flowable Composite.........................*/
  flowableDesc:ProductDesc[]=[
    {text:"Highly polishable, 67% filled, esthetic composite used for all types of cavity preparations."},
    {text:"The composite has a nominal particle size of 0.7 micron. An ideal choice for both anterior and posterior applications and also as a pit and fissure sealant."},
    {text:"Ideal for repairing small defects and filling in cervical gaps caused by toothbrush erosion and receding gums."},
    {text:"Also used to eliminate undercuts prior to taking impressions for composite inlays and to line cavities and fill in difficult access areas prior to placing condensable composites."},
    {text:"Easy to polish to a high luster. Has high compressive strength as well as low shrinkage, resists wear and staining. Radiopaque for easy identification in radiographs."},
  ];
  flowableBenefits:PrimeProductBenefitAndFeature[]=[
    {point:"Available in 18 Vita shades"},
    {point:"Micro-Hybrid formula provides strength and polishability"},
    {point:"Fast, deep light activation in only 30 seconds for lighter shades."},
    {point:"Deep light activation in 40 seconds for darker shades."},
    {point:"Radiopaque"},
    {point:"Fluoride releasing"},
    {point:"Low shrinkage"},
    {point:"High compressive strength"},
  ];
  flowableTechData:PrimeProductTechData[]=[
    {variable:"Weight/Volume", value:"67%/54%"},
    {variable:"Compressive Strength", value:"290 MPa"},
    {variable:"Diametral Tensile Strength", value:"47 MPa"},
    {variable:"Flexural Strength", value:"125 MPa"},
  ];
  /*......................Porcelain Bond Enhancer.........................*/
  bondEnhanceDesc:ProductDesc[]=[
    {text:"A silane coupling agent that treats porcelain to give stronger bonds, typically used after the porcelain surface has been etched with Prime-Dent© porcelain etch gel"},
  ];
  /*......................DENSELL Endo-G-Fill.........................*/
  densellEndo:ProductDesc[]=[
    {text:"Grossman Type formula enhanced Root canal sealer"}
  ];
  /*......................RISTEA Glass Ionomer Cement.........................*/
  risteaGlassDesc:ProductDesc[]=[
    {text:"This product can be used for the cementation of crowns fixed partial, inlays and the restorative filling of glass ? & ?, and is highly biocompatible and good in marginal seal"},
    {text:"Available in 3 shades: 1-3 from light to dark"}
  ];
  risteaGlassTable:RisteaProductInfo[]=[
    {res1:"AM31101",res2:"20g powder / bottle",res3:"300bottles/ctn"},
    {res1:"AM31102",res2:"15ml liquid / bottle",res3:"150bottles/ctn"},
    {res1:"AM31103",res2:"3 bottles powder +3 bottles liquid / set",res3:"50sets/ctn"},
  ]
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type']; 
    this.productTitle=type;
    if(type=='Porcelain Etchant Gel'){
      this.productImg="prime dental/1_4_70";
      this.productDesc=this.primePorcelainGellDesc;
      this.isHr=false;
    }
    else if(type=='PRIME-GEL'){
      this.productImg="prime dental/310Nnpg4vZL";
      this.productDesc=this.primeGelDesc;
      this.isHr=false;
    }
    else if(type=='Dual Cure Composite Luting Cement'){
      this.productImg="prime dental/f80b3dd9aad54d2d2e4621072ac98fac";
      this.productDesc=this.dualCureCementDesc;
      this.benefitAndFeature=this.dualCureCementBenefits;
      this.techData=this.dualCureCementTechData;
      this.techDataText=this.dualCureCementTechDataText;
      this.isBenefit=true;
      this.isTech=true;
      this.isTechText=true;
    }
    else if(type=='Hybrid Composite'){
      this.productImg="prime dental/Hybrid Composite Kit";
      this.productDesc=this.hybridDesc;
      this.benefitAndFeature=this.hybridBenefits;
      this.isBenefit=true;
    }
    else if(type=='Prime Core DC'){
      this.productImg="prime dental/Prime Core DC Automix Syringe Core Build Up Material (Prime Dental)";
      this.productDesc=this.primeCoreDesc;
      this.isHr=false;
    }
    else if(type=='Prime Crown'){
      this.productImg="prime dental/prime-crown-prime-dent (1)";
      this.productDesc=this.crownDesc;
      this.benefitAndFeature=this.crownBenefits;
      this.techData=this.crownTechData;
      this.isBenefit=true;
      this.isTech=true;
    }
    else if(type=='Flowable Composite'){
      this.productImg="prime dental/prime-dent-flowable-004-010A1";
      this.productDesc=this.flowableDesc;
      this.benefitAndFeature=this.flowableBenefits;
      this.techData=this.flowableTechData;
      this.isBenefit=true;
      this.isTech=true;
    }
    else if(type=='Porcelain Bond Enhancer'){
      this.productImg="prime dental/s-l1600";
      this.productDesc=this.bondEnhanceDesc;
      this.isHr=false;
    }
    else if(type=='Endo-G-Fill'){
      this.productImg="densell/Endo_Densell_4c1648522d21d";
      this.productDesc=this.densellEndo;
      this.isHr=false;
    }
    else if(type=='Glass Ionomer Cement'){
      this.tableHeader=["Code","Description","Packing"];
      this.productImg="ristea/Glass Ionomer Cement";
      this.productDesc=this.risteaGlassDesc;
      this.risteaInfo=this.risteaGlassTable;
      this.isTable=true;
    }
  }
onBack(){
    this.router.navigate(['/Products/CrownAndBridges'])
  }
}
