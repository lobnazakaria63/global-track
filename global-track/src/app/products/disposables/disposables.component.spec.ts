import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisposablesComponent } from './disposables.component';

describe('DisposablesComponent', () => {
  let component: DisposablesComponent;
  let fixture: ComponentFixture<DisposablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisposablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisposablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
