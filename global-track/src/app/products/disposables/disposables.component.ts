import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Products} from "../../../models/products";
@Component({
  selector: 'app-disposables',
  templateUrl: './disposables.component.html',
  styleUrls: ['./disposables.component.css']
})
export class DisposablesComponent implements OnInit {

  products:Products[]=[
    {
      image:"Promisee/autoclavable impression trays",
      key:"pPromisee",
      name:'Autoclavable Impression Trays',
      company:"Citisen"
    },
    {
      image:"Promisee/bur holder",
      key:"pPromisee",
      name:'Bur Holder Box',
      company:"Citisen"
    },
    {
      image:"Promisee/cheek retractor 2",
      key:"pPromisee",
      name:'Cheek Retractor 2',
      company:"Citisen"
    },
    {
      image:"Promisee/cheek retractor 3",
      key:"pPromisee",
      name:'Cheek Retractor 3',
      company:"Citisen"
    },
    {
      image:"Promisee/cheek retractor",
      key:"pPromisee",
      name:'Cheek Retractor',
      company:"Citisen"
    },
    {
      image:"Promisee/Compule Dispenser Gun",
      key:"pPromisee",
      name:'Compule Dispenser Gun',
      company:"Citisen"
    },
    {
      image:"Promisee/Cotton roll dispenser",
      key:"pPromisee",
      name:'Drawer Type Cotton Roll Dispenser',
      company:"Citisen"
    },
    {
      image:"Promisee/Denture Box",
      key:"pPromisee",
      name:'Denture Box',
      company:"Citisen"
    },
    {
      image:"Promisee/Dispenser-Gun-ConvertImage",
      key:"pPromisee",
      name:'Dispenser Gun',
      company:"Citisen"
    },
    {
      image:"Promisee/Disposable-Autoclavable-Dental-Plastic-Impression-Trays-for",
      key:"pPromisee",
      name:'Disposable Impression Trays',
      company:"Citisen"
    },
    {
      image:"Promisee/monoart_einmaltrays_131439_1",
      key:"pPromisee",
      name:'Disposable Tray',
      company:"Citisen"
    },
    {
      image:"Promisee/Ruier-Dental-Protect-Face-Guard-with-10pcs",
      key:"pPromisee",
      name:'Face Mask',
      company:"Citisen"
    },
    {
      image:"Promisee/Face shield",
      key:"pPromisee",
      name:'Protective Shields',
      company:"Citisen"
    },
    {
      image:"Promisee/Glass Dappen Dish",
      key:"pPromisee",
      name:'Glass Dappen Dish',
      company:"Citisen"
    },
    {
      image:"Promisee/SQ6B0027",
      key:"pPromisee",
      name:'Micro Applicator Brush',
      company:"Citisen"
    },
    {
      image:"Promisee/Mixing Tips",
      key:"pPromisee",
      name:'HP Mixing Tip',
      company:"Citisen"
    },
    {
      image:"Promisee/SQ6B0034",
      key:"pPromisee",
      name:'Mouth Prop & Tongue Guard',
      company:"Citisen"
    },
    {
      image:"Promisee/SQ6B0035",
      key:"pPromisee",
      name:'Mouth Prop',
      company:"Citisen"
    },
    {
      image:"Promisee/PLS-B009",
      key:"pPromisee",
      name:'PLS-B009',
      company:"Citisen"
    },
    {
      image:"Promisee/spatula",
      key:"pPromisee",
      name:'Spatula',
      company:"Citisen"
    },
    {
      image:"Promisee/SQ6B0025ed",
      key:"pPromisee",
      name:'Surgical Aspirator Tips',
      company:"Citisen"
    },
    {
      image:"Promisee/Wooden wedges",
      key:"pPromisee",
      name:'Wooden Wedges',
      company:"Citisen"
    },
    {
      image:"medprotect/barrier film 2",
      key:"pMedProtect",
      name:'Barrier Film 2',
      company:"MedProtect"
    },
    {
      image:"medprotect/barrier film",
      key:"pMedProtect",
      name:'Barrier Film',
      company:"MedProtect"
    },
    {
      image:"medprotect/face mask 2",
      key:"pMedProtect",
      name:'Face Mask 2',
      company:"MedProtect"
    },
    {
      image:"medprotect/face mask",
      key:"pMedProtect",
      name:'Face Mask',
      company:"MedProtect"
    },
    {
      image:"medprotect/pompe-salive-medistock",
      key:"pMedProtect",
      name:'Saliva Ejector',
      company:"MedProtect"
    },
    {
      image:"larident/alluminium instruments tray",
      key:"pLarident",
      name:'Alluminium Instruments Tray',
      company:"Larident"
    },
    {
      image:"larident/autoclavable amlgam carrier",
      key:"pLarident",
      name:'Autoclavable Amlgam Carrier',
      company:"Larident"
    },
    {
      image:"larident/autoclavable plastic composite instruments",
      key:"pLarident",
      name:'Autoclavable Plastic Composite Instruments',
      company:"Larident"
    },
    {
      image:"maquira/AMALGAM CARRIER",
      key:"pMaquira",
      name:'AMALGAM CARRIER',
      company:"Maquira"
    },
    {
      image:"maquira/ARCH FOLDING ADULT",
      key:"pMaquira",
      name:'ARCH FOLDING ADULT',
      company:"Maquira"
    },
    {
      image:"maquira/ARCH FOLDING KID",
      key:"pMaquira",
      name:'ARCH FOLDING KID',
      company:"Maquira"
    },
    {
      image:"maquira/ARCH OF SIMPLE OSTBY ADULT",
      key:"pMaquira",
      name:'ARCH OF SIMPLE OSTBY ADULT',
      company:"Maquira"
    },
    {
      image:"maquira/ARCH OF SIMPLE OSTBY KID",
      key:"pMaquira",
      name:'ARCH OF SIMPLE OSTBY KID',
      company:"Maquira"
    },
    {
      image:"maquira/ASPIRATOR WITH BONE COLETOR",
      key:"pMaquira",
      name:'ASPIRATOR WITH BONE COLETOR',
      company:"Maquira"
    },
    {
      image:"maquira/BIO-INDICADOR",
      key:"pMaquira",
      name:'BIO-INDICADOR',
      company:"Maquira"
    },
    {
      image:"maquira/ENDO + AUTOCLAVABLE",
      key:"pMaquira",
      name:'ENDO + AUTOCLAVABLE',
      company:"Maquira"
    },
    {
      image:"maquira/FILM HOLDER CONE AUTOCLAVABLE",
      key:"pMaquira",
      name:'FILM HOLDER CONE AUTOCLAVABLE',
      company:"Maquira"
    },
    {
      image:"maquira/FLUORIDE GEL",
      key:"pMaquira",
      name:'FLUORIDE GEL',
      company:"Maquira"
    },
    {
      image:"maquira/ORTHODONTIC WAX",
      key:"pMaquira",
      name:'ORTHODONTIC WAX',
      company:"Maquira"
    },
    {
      image:"maquira/POLYESTER ABRASIVE STRIP AIRON",
      key:"pMaquira",
      name:'POLYESTER ABRASIVE STRIP AIRON',
      company:"Maquira"
    },
    {
      image:"maquira/POLYESTER STRIP",
      key:"pMaquira",
      name:'POLYESTER STRIP',
      company:"Maquira"
    },
    {
      image:"maquira/PROPHYLACTIC PASTE",
      key:"pMaquira",
      name:'PROPHYLACTIC PASTE',
      company:"Maquira"
    },
    {
      image:"maquira/RETRACTOR1",
      key:"pMaquira",
      name:'RETRACTOR #0',
      company:"Maquira"
    },
    {
      image:"maquira/RETRACTORtwo",
      key:"pMaquira",
      name:'RETRACTOR #00',
      company:"Maquira"
    },
    {
      image:"maquira/RETRACTOR3",
      key:"pMaquira",
      name:'RETRACTOR #000',
      company:"Maquira"
    },
    {
      image:"maquira/STAINLESS STEEL MATRIX BANDS",
      key:"pMaquira",
      name:'STAINLESS STEEL MATRIX BANDS',
      company:"Maquira"
    },
    {
      image:"maquira/VISUCARIE",
      key:"pMaquira",
      name:'VISUCARIE',
      company:"Maquira"
    },
    {
      image:"maquira/YOUNG ARCH",
      key:"pMaquira",
      name:'YOUNG ARCH',
      company:"Maquira"
    },
    {
      image:"W.R.Rayson/W.R",
      key:"pW.R.Rayson",
      name:'Articulating Paper',
      company:"W.R.Rayson"
    },
  ];
  
  filter1: any={key:'Promisee'};
  filter2:any={key:'W.R.Rayson'};
  filter3:any={key:'MedProtect'};
  filter4: any={key:'Maquira'};
  filter5:any={key:'Larident'};
  filterAll:any={key:'p'};
  filter=this.filterAll;
  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
    
  }
  filterAllFunc(){
    this.filter=this.filterAll;
  }
  filterOneFunc(){
    this.filter=this.filter1;
  }
  filterTwoFunc(){
    this.filter=this.filter2;
  }
  filterThreeFunc(){
    this.filter=this.filter3;
  }
  filterFourFunc(){
    this.filter=this.filter4;
  }
  filterFiveFunc(){
    this.filter=this.filter5;
  }
}
