import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaridentComponent } from './larident.component';

describe('LaridentComponent', () => {
  let component: LaridentComponent;
  let fixture: ComponentFixture<LaridentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaridentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaridentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
