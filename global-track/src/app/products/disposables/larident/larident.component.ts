import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CodeDesc, Description } from '../../../../models/laridentProductsDet';

@Component({
  selector: 'app-larident',
  templateUrl: './larident.component.html',
  styleUrls: ['./larident.component.css']
})
export class LaridentComponent implements OnInit {
  productTitle:string;
  laridentImg:string;
  laridentDetail:CodeDesc[];
  laridentDesc:Description;
  isCode=false;
  isDesc=false;
  /*...............Alluminium Instruments Tray..........................*/
  lariAlluDetail:CodeDesc[]=[
    {variable:"Q17",value:"Vassoio alluminio anodizzato"},
    {variable:"Q17.1",value:"Coperchio per Q 17 colore alluminio"},
    {variable:"Q24",value:"Rastrelliera per 8 strumenti"},
    {variable:"Q31",value:"Tray completo vassoio, coperchio, rastrelliera"},
  ];
  /*...............Autoclavable Amlgam Carrier..........................*/
  autoAmDetail:CodeDesc[]=[
    {variable:"A11",value:"Siringa curva autoclavabile"},
    {variable:"A12",value:"Siringa diritta autoclavabile"},
    {variable:"A14",value:"Siringa curva 110° autoclavabile"},
    {variable:"A15",value:"Mortaio per amalgama bianco autoclavabile, guarnizione antisdrucciolo"},
    {variable:"A11.1",value:"Becco ricambiato curvo autoclavabile"},
    {variable:"A12.1",value:"Becco ricambiato diritto autoclavabile"},
    {variable:"A14.1",value:"Becco ricambiato 110° autoclavabile"},
  ];
  /*...............Autoclavable Plastic Composite Instruments..........................*/
  autoPlasticDesc:Description={
    text:"Series 5 spatulas for composite, in autoclavable plastic"
  };
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type']; 
    this.productTitle=type;
    if(type=='Alluminium Instruments Tray'){
      this.laridentImg="larident/alluminium instruments tray";
      this.laridentDetail=this.lariAlluDetail;
      this.isCode=true;
      
    }
    else if(type=='Autoclavable Amlgam Carrier'){
      this.laridentImg="larident/autoclavable amlgam carrier";
      this.laridentDetail=this.autoAmDetail;
      this.isCode=true;
    }
    else if(type=='Autoclavable Plastic Composite Instruments'){
      this.laridentImg="larident/autoclavable plastic composite instruments";
      this.laridentDesc=this.autoPlasticDesc;
      this.isDesc=true;
    }
  }
  onBack(){
    this.router.navigate(['/Products/Disposables'])
  }
}
