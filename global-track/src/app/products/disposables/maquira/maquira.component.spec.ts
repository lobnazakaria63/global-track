import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaquiraComponent } from './maquira.component';

describe('MaquiraComponent', () => {
  let component: MaquiraComponent;
  let fixture: ComponentFixture<MaquiraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaquiraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaquiraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
