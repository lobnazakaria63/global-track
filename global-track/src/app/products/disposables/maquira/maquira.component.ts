import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Presentation, Advantage } from '../../../../models/maquiraProductDet';

@Component({
  selector: 'app-maquira',
  templateUrl: './maquira.component.html',
  styleUrls: ['./maquira.component.css']
})
export class MaquiraComponent implements OnInit {
  productTitle:string;
  maquiraImg:string;
  isHr=true;
  isprePoint=false;
  maquiraPresentation:Presentation[];
  maquiraAdvantage:Advantage[];
  maquiraPrePoint:Advantage[];
  /*..........................AMALGAM CARRIER.....................................*/
  amalgamPresentation:Presentation[]=[
    {text:"It is a tool used only for dentists which helps prepare the root canal should be used only for transport and ion of amalgam in the dental cavity by the professional according to his/her ability and scientific knowledge."}
  ];
  amalgamAdvantage:Advantage[]=[
    {point:"Assist prepare the root cana"},
    {point:"Used only for transporting and ing the amalgam in dental cavity"},
    {point:"Autoclave Sterilization up to 134° C"},
    {point:"Available in Green and Pink"},
  ];
  /*..........................ARCH FOLDING ADULT.....................................*/
  archFoldAdultPresentation:Presentation[]=[
    {text:"The Ostby Arch has a system of folds which improves access to oral cavity, and it was produced using very resistant material to various sterilization procedures."},
    {text:" Its use is indicated only for support of the rubber dam sheet for a rubber dam isolation of the tooth to be restored. Safe use in patients."},
  ];
  archFoldAdultAdvantage:Advantage[]=[
    {point:"Used to support the rubber dam sheet for a rubber dam isolation of the tooth to be restored"},
    {point:"Foldable Arch"},
    {point:"Autoclave Sterilization up to 121°C"},
  ];
   /*..........................ARCH FOLDING KID.....................................*/
   archFoldKidPresentation:Presentation[]=[
    {text:"The Ostby Arch has a system of folds which improves access to oral cavity, and it was produced using very resistant material to various sterilization procedures."},
    {text:" Its use is indicated only for support of the rubber dam sheet for a rubber dam isolation of the tooth to be restored. Safe use in patients."},
  ];
  archFoldKidAdvantage:Advantage[]=[
    {point:"Used to support the rubber dam sheet for a rubber dam isolation of the tooth to be restored"},
    {point:"Foldable Arch"},
    {point:"Autoclave Sterilization up to 121°C"},
  ];
  /*..........................ARCH OF SIMPLE OSTBY ADULT.....................................*/
  archOstAdultPresentation:Presentation[]=[
    {text:"The Arch of Simple Ostby was developed to fix the rubber sheet, providing a rubber dam in the tooth to be restored. This product was developed by highly resistant material to autoclave sterilization. Ensure the safe use for the patient."},
  ];
  archOstAdultAdvantage:Advantage[]=[
    {point:"Used to support the rubber sheet for absolute isolation of the tooth to be restored."},
    {point:"Greater flexibility and easy use"},
    {point:"Avaible in the following sizes: Adult and Child"},
    {point:"Sterilization Autoclave at 121°C."},
  ];
  /*..........................ARCH OF SIMPLE OSTBY KID.....................................*/
  archOstKidPresentation:Presentation[]=[
    {text:"The Arch of Simple Ostby was developed to fix the rubber sheet, providing a rubber dam in the tooth to be restored. This product was developed by highly resistant material to autoclave sterilization. Ensure the safe use for the patient."},
  ];
  archOstKidAdvantage:Advantage[]=[
    {point:"Used to support the rubber sheet for absolute isolation of the tooth to be restored."},
    {point:"Greater flexibility and easy use"},
    {point:"Avaible in the following sizes: Adult and Child"},
    {point:"Sterilization Autoclave at 121°C."},
  ];
  /*..........................ASPIRATOR WITH BONE COLETOR.....................................*/
  aspiratorPresentation:Presentation[]=[
    {text:"The surgical aspirator Maquira offers higher level of security in the procedures, reducing the chances of contamination of both the professional and the patients."},
    {text:"Used in Periodontics, Implants and Maxillofacial. Composition: Main tube in ABS, Tips and collector inpolypropileno."},
  ];
  aspiratorAdvantage:Advantage[]=[
    {point:"Avoids the bone particles damaging the Suction tube"},
    {point:"Packaging sterilized individually (Ethylene Oxide"},
    {point:"2 Collectors per packaging"},
    {point:"Perfect fit in hose"},
    {point:"Air outlet for constant suction"},
    {point:"Box containing 10 saliva ejectors + 10 filters"},
  ];
  /*..........................BIO-INDICADOR.....................................*/
  bioIndicPresentation:Presentation[]=[
    {text:"Indicated for periodical monitoring of sterilization cycles and in validation of autoclaves. Sterilization is essential in the control of infections in the various health establishments."},
    {text:"The BIOINDICATOR aims to monitor steam sterilization, providing greater reliability in this process, through bacterial spores resistant to heat, the Geobacillus stearothermophilus ATCC 7953."},
  ];
  bioIndicAdvantage:Advantage[]=[
    {point:"Presents the test results in 24 hours"},
    {point:"Precise indication"},
    {point:"Easy to use, no need sophistic laboratory testing or analysis"},
    {point:"Product is not harmful to health"},
    {point:"Available in packaging: 10 plastic bottles containing slips of paper with Geobacillus stearothermophillus ATCC 7953 spores and sealed glass ampoules."},
    
  ];
  /*..........................ENDO + AUTOCLAVABLE.....................................*/
  endoPresentation:Presentation[]=[
    {text:"The ENDO + POSITIONER CONE is a product used to assist in the orientation of the angle of the X-ray unit dental procedures in periodontics, endodontics and implantology with or without rubber dam, resulting in standardized radiographic images and sharp"},
  ];
  endoAdvantage:Advantage[]=[
    {point:"Used to position radiographic films"},
    {point:"Clear photographic image"},
    {point:"Saves time and radiographic films"},
    {point:"3 functions in 1: Periodontics / Endodontics / Implant"},
    {point:"Available in Chemical Est"},
  ];
  /*..........................FILM HOLDER CONE AUTOCLAVABLE.....................................*/
  filmHolderPresentation:Presentation[]=[
    {text:"CONE Film Holder is a product used to help set the angle of X-rays dental equipment, resulting in standardized and clear radiographic images. "},
    {text:"Box with 1 kit"},
    {text:"Each kit contains: "},
  ];
  filmHolderPrePoint:Advantage[]=[
    {point:"Positioner for upper and lower incisors and canines."},
    {point:"Positioner right upper molar and lower left molar."},
    {point:"Positioner left upper molar and lower right molar"},
    {point:"Positioner for BITE WINGS interproximal radiographs."},
    {point:"Pot for storage."},
    {point:" Bite device (03 units)."},
  ];
  filmHolderAdvantage:Advantage[]=[
    {point:"Used to position radiographic films"},
    {point:"Clear photographic image"},
    {point:"Saves time and radiographic films"},
    {point:"Periapical diagnostic"},
    {point:"Available at Adult, Child and Universal sizes"},
    {point:"Available at Autoclavable"},
  ];
  /*..........................FLUORIDE GEL.....................................*/
  fluoridePresentation:Presentation[]=[
    {text:"MAQUIRA FLUORIDE is a product that helps prevent tooth decay by slowing the breakdown of enamel and speeding up the natural process of remineralization of enamel filaments."},
  ];
  fluorideAdvantage:Advantage[]=[
    {point:"Reduction of demineralization of enamel"},
    {point:"Decrease the number and the potential for oral microorganisms"},
    {point:"Encourage the rebuilding of impaired enamel"},
    {point:"Available in Acidulated and Neutral Gel"},
    {point:"Acidulated flavors Cherry, Mint and Tutti-frutti"},
  ];
  /*..........................ORTHODONTIC WAX.....................................*/
  orthWaxPresentation:Presentation[]=[
    {text:"Orthodontic Wax is a product used in orthodontic appliances, which helps protect the lips against mechanical shock."},
    {text:" The material is made non-toxic paraffin which allows a great flexibility, making it easy to adapt to devices and providing greater comfort for the patient."},
    {text:"Box of 05 sticks of solid wax"},
  ];
  orthWaxAdvantage:Advantage[]=[
    {point:"Protects the cheek against wounds caused by brackets"},
    {point:"Because it is transparent, it does not affect the aesthetics"},
    ];
  /*..........................POLYESTER ABRASIVE STRIP AIRON.....................................*/
  polyStripAironPresentation:Presentation[]=[
    {text:"The Polyester Abrasive Strip aims finishing and polishing of dental restorations. Made polyester and aluminum oxide sanding, each strips has two parts: one thin and one thick."},
  ];
  polyStripAironAdvantage:Advantage[]=[
    {point:"The polyester sanding strip provides a better finishing and polishing of dental restorations"},
    {point:"Aids having a perfect Interproximal restoration"},
    {point:"Each product sander individually has two parts constituted of: one fine and the other thick"},
  ];
  /*..........................POLYESTER STRIP.....................................*/
  polyStripPresentation:Presentation[]=[
    {text:"AIRON POLYESTER STRIP is used to separate the tooth to be restored its neighbor. To obtain a perfect restoration it is necessary to use the strip of polyester, with the function of confining under pressure, the restorative material in the cavity"},
    {text:" Protects the cement during the gelation reaction, resulting in hardening of the material, loss or absorption of moisture present in the site."},
  ];
  polyStripAdvantage:Advantage[]=[
    {point:"Product full transparent"},
    {point:"Used to separate the tooth to be restored its neighbor"},
    {point:"Perform perfect restoration confining under pressure"},
    {point:"Measures: 10mm X 120 mm x 0,05 mm"},
  ];
  /*..........................PROPHYLACTIC PASTE.....................................*/
  prophyPresentation:Presentation[]=[
    {text:"Maquira Prophylactic Paste Shine is abrasive enough to remove efficiently all types of waste the tooth surface without causing undesired abrasion of the enamel, dentin or cement"},
    {text:"Besides acting as a cleaning agent, it gives the hard tissue an aesthetic and highly polished appearance."},
  ];
  prophyAdvantage:Advantage[]=[
    {point:"Indicated to remove inpurities which are on the enamel"},
    {point:"Formulation WITHOUT OIL"},
    {point:"Good consistency/suitable texture"},
    {point:"Does not damage the Counterangle handpiece (instrument used for application of the paste)"},
    {point:"Easy to handle plastic package (perfect closure to prevent drying)"},
    {point:"Contains Fluorine"},
    {point:"Available in flavors: Mint and Tutti-Frutti."},
  ];
  /*..........................RETRACTOR #0.....................................*/
  retaOnePresentation:Presentation[]=[
    {text:"The retractor Maquira is a cord made to a gingival retraction, made 100% cotton, not impregnated."},
    {text:"This cord is knitted into interlocking chains to facilitate easy packing of cord into sulcus."},
    {text:"# 0 THIN."},
  ];
  retaOneAdvantage:Advantage[]=[
    {point:"Gingival retraction cord made of 100% cotton, not impregnated"},
    {point:"Increased absorption of hemostatic materials"},
    {point:"Knitted cord shows more flexibity"},
    {point:"Product packaging designed to facilitate the performance of professional dentist at the time of handling"},
    {point:"Developed in wire sizes: #000, #00, #0, #1, #2."},
  ];
  /*..........................RETRACTOR #00.....................................*/
  retaTwoPresentation:Presentation[]=[
    {text:"The retractor Maquira is a cord made to a gingival retraction, made 100% cotton, not impregnated."},
    {text:"This cord is knitted into interlocking chains to facilitate easy packing of cord into sulcus."},
    {text:"# 00 EXTRA THIN."},
  ];
  retaTwoAdvantage:Advantage[]=[
    {point:"Gingival retraction cord made of 100% cotton, not impregnated"},
    {point:"Increased absorption of hemostatic materials"},
    {point:"Knitted cord shows more flexibity"},
    {point:"Product packaging designed to facilitate the performance of professional dentist at the time of handling"},
    {point:"Developed in wire sizes: #000, #00, #0, #1, #2."},
  ];
  /*..........................RETRACTOR #000.....................................*/
  retaThreePresentation:Presentation[]=[
    {text:"The retractor Maquira is a cord made to a gingival retraction, made 100% cotton, not impregnated."},
    {text:"This cord is knitted into interlocking chains to facilitate easy packing of cord into sulcus."},
    {text:"# 000 ULTRA EXTRA THIN."},
  ];
  retaThreeAdvantage:Advantage[]=[
    {point:"Gingival retraction cord made of 100% cotton, not impregnated"},
    {point:"Increased absorption of hemostatic materials"},
    {point:"Knitted cord shows more flexibity"},
    {point:"Product packaging designed to facilitate the performance of professional dentist at the time of handling"},
    {point:"Developed in wire sizes: #000, #00, #0, #1, #2."},
  ];
   /*..........................STAINLESS STEEL MATRIX BANDS.....................................*/
   stainlessPresentation:Presentation[]=[
     {text:"The MATRIX BAND is made of stainless steel. Recommended for dental reconstruction, it has a uniform surface. Its edges will not hurt the patient, easy to apply and remove without damaging the restoration."},
     {text:" Suitable for reconstruction of posterior teeth. Promotes the proximal contour and is rigid enough to not unglue during the condensation of amalgam."},
   ];
   stainlessAdvantage:Advantage[]=[
     {point:"Made of stainless steel"},
     {point:"It has a uniform surface"},
     {point:"Edges do not hurt the patient"},
     {point:"Flexible - promotes the proximal contour"},
     {point:"Easy removal without damagind the restoration"},
     {point:"Available in the following measures: 0.05mmx5mmx50cm and 0.05mmx7mmx50cm"},
   ];
   /*..........................VISUCARIE.....................................*/
   visuPresentation:Presentation[]=[
    {text:"Visucarie is indicated for visualization and identification of decayed tissue."},
  ];
  visuAdvantage:Advantage[]=[
    {point:"Excelent exposure os the carious tissue"},
    {point:"Used if there is doubt if the dentin or enamel has caries or not"},
    {point:"Acts in the caries which is not black (assist to identify)"},
    {point:"If there is no caries, it does not stain"},
    {point:"Indicated for any age"},
  ];
   /*..........................YOUNG ARCH.....................................*/
   youngArchPresentation:Presentation[]=[
     {text:"The Young Arch was developed to fix the rubber sheet, providing a rubber dam in the tooth to be restored. This product was developed by highly resistant material to autoclave sterilization. Ensure the safe use for the patient."}
   ];
   youngArchAdvantage:Advantage[]=[
     {point:"Indicated for absolute isolation os the tooth to be treated"},
     {point:"Isolation of teeth: Lower"},
     {point:"Produced in plastic material: Greater flexibility"},
     {point:"Sterilization Autoclave at 121°C"},
   ];
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type']; 
    this.productTitle=type;
    if(type=='AMALGAM CARRIER'){
      this.maquiraImg="maquira/AMALGAM CARRIER";
      this.maquiraPresentation=this.amalgamPresentation;
      this.maquiraAdvantage=this.amalgamAdvantage;
    }
    else if(type=='ARCH FOLDING ADULT'){
      this.maquiraImg="maquira/ARCH FOLDING ADULT";
      this.maquiraPresentation=this.archFoldAdultPresentation;
      this.maquiraAdvantage=this.archFoldAdultAdvantage;
    }
    else if(type=='ARCH FOLDING KID'){
      this.maquiraImg="maquira/ARCH FOLDING KID";
      this.maquiraPresentation=this.archFoldKidPresentation;
      this.maquiraAdvantage=this.archFoldKidAdvantage;
    }
    else if(type=='ARCH OF SIMPLE OSTBY ADULT'){
      this.maquiraImg="maquira/ARCH OF SIMPLE OSTBY ADULT";
      this.maquiraPresentation=this.archOstAdultPresentation;
      this.maquiraAdvantage=this.archOstAdultAdvantage;
    }
    else if(type=='ARCH OF SIMPLE OSTBY KID'){
      this.maquiraImg="maquira/ARCH OF SIMPLE OSTBY KID";
      this.maquiraPresentation=this.archOstKidPresentation;
      this.maquiraAdvantage=this.archOstKidAdvantage;
    }
    else if(type=='ASPIRATOR WITH BONE COLETOR'){
      this.maquiraImg="maquira/ASPIRATOR WITH BONE COLETOR";
      this.maquiraPresentation=this.aspiratorPresentation;
      this.maquiraAdvantage=this.aspiratorAdvantage;
    }
    else if(type=='BIO-INDICADOR'){
      this.maquiraImg="maquira/BIO-INDICADOR";
      this.maquiraPresentation=this.bioIndicPresentation;
      this.maquiraAdvantage=this.bioIndicAdvantage;
    }
    else if(type=='ENDO + AUTOCLAVABLE'){
      this.maquiraImg="maquira/ENDO + AUTOCLAVABLE";
      this.maquiraPresentation=this.endoPresentation;
      this.maquiraAdvantage=this.endoAdvantage;
    }
    else if(type=='FILM HOLDER CONE AUTOCLAVABLE'){
      this.maquiraImg="maquira/FILM HOLDER CONE AUTOCLAVABLE";
      this.maquiraPresentation=this.filmHolderPresentation;
      this.maquiraAdvantage=this.filmHolderAdvantage;
      this.maquiraPrePoint=this.filmHolderPrePoint;
      this.isprePoint=true;
    }
    else if(type=='FLUORIDE GEL'){
      this.maquiraImg="maquira/FLUORIDE GEL";
      this.maquiraPresentation=this.fluoridePresentation;
      this.maquiraAdvantage=this.fluorideAdvantage;
    }
    else if(type=='ORTHODONTIC WAX'){
      this.maquiraImg="maquira/ORTHODONTIC WAX";
      this.maquiraPresentation=this.orthWaxPresentation;
      this.maquiraAdvantage=this.orthWaxAdvantage;
    }
    else if(type=='POLYESTER ABRASIVE STRIP AIRON'){
      this.maquiraImg="maquira/POLYESTER ABRASIVE STRIP AIRON";
      this.maquiraPresentation=this.polyStripAironPresentation;
      this.maquiraAdvantage=this.polyStripAironAdvantage;
    }
    else if(type=='POLYESTER STRIP'){
      this.maquiraImg="maquira/POLYESTER STRIP";
      this.maquiraPresentation=this.polyStripPresentation;
      this.maquiraAdvantage=this.polyStripAdvantage;
    }
    else if(type=='PROPHYLACTIC PASTE'){
      this.maquiraImg="maquira/PROPHYLACTIC PASTE";
      this.maquiraPresentation=this.prophyPresentation;
      this.maquiraAdvantage=this.prophyAdvantage;
    }
    else if(type=='RETRACTOR #0'){
      this.maquiraImg="maquira/RETRACTOR1";
      this.maquiraPresentation=this.retaOnePresentation;
      this.maquiraAdvantage=this.retaOneAdvantage;
    }
    else if(type=='RETRACTOR #00'){
      this.maquiraImg="maquira/RETRACTORtwo";
      this.maquiraPresentation=this.retaTwoPresentation;
      this.maquiraAdvantage=this.retaTwoAdvantage;
    }
    else if(type=='RETRACTOR #000'){
      this.maquiraImg="maquira/RETRACTOR3";
      this.maquiraPresentation=this.retaThreePresentation;
      this.maquiraAdvantage=this.retaThreeAdvantage;
    }
    else if(type=='STAINLESS STEEL MATRIX BANDS'){
      this.maquiraImg="maquira/STAINLESS STEEL MATRIX BANDS";
      this.maquiraPresentation=this.stainlessPresentation;
      this.maquiraAdvantage=this.stainlessAdvantage;
    }
    else if(type=='VISUCARIE'){
      this.maquiraImg="maquira/VISUCARIE";
      this.maquiraPresentation=this.visuPresentation;
      this.maquiraAdvantage=this.visuAdvantage;
    }
    else if(type=='YOUNG ARCH'){
      this.maquiraImg="maquira/YOUNG ARCH";
      this.maquiraPresentation=this.youngArchPresentation;
      this.maquiraAdvantage=this.youngArchAdvantage;
    }
  }
  onBack(){
    this.router.navigate(['/Products/Disposables'])
  }
}
