import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedProtectComponent } from './med-protect.component';

describe('MedProtectComponent', () => {
  let component: MedProtectComponent;
  let fixture: ComponentFixture<MedProtectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedProtectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedProtectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
