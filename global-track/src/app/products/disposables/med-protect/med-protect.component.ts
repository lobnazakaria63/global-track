import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-med-protect',
  templateUrl: './med-protect.component.html',
  styleUrls: ['./med-protect.component.css']
})
export class MedProtectComponent implements OnInit {
  productTitle:string;
  MedProtectImg:string;
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type']; 
    this.productTitle=type;
    if(type=='Barrier Film 2'){
      this.MedProtectImg="medprotect/barrier film 2";
    }
    else if(type=='Barrier Film'){
      this.MedProtectImg="medprotect/barrier film";
    }
    else if(type=='Face Mask 2'){
      this.MedProtectImg="medprotect/face mask 2";
    }
    else if(type=='Face Mask'){
      this.MedProtectImg="medprotect/face mask";
    }
    else if(type=='Saliva Ejector'){
      this.MedProtectImg="medprotect/pompe-salive-medistock";
    }
  }
  onBack(){
    this.router.navigate(['/Products/Disposables'])
  }
}
