import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-promisee',
  templateUrl: './promisee.component.html',
  styleUrls: ['./promisee.component.css']
})
export class PromiseeComponent implements OnInit {
  productTitle:string;
  promiseeImg:string;
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type']; 
    this.productTitle=type;
    if(type=='Autoclavable Impression Trays'){
      this.promiseeImg="Promisee/autoclavable impression trays";
    }
    else if(type=='Bur Holder Box'){
      this.promiseeImg="Promisee/bur holder";
    }
    else if(type=='Cheek Retractor 2'){
      this.promiseeImg="Promisee/cheek retractor 2";
    }
    else if(type=='Cheek Retractor 3'){
      this.promiseeImg="Promisee/cheek retractor 3";
    }
    else if(type=='Cheek Retractor'){
      this.promiseeImg="Promisee/cheek retractor";
    }
    else if(type=='Compule Dispenser Gun'){
      this.promiseeImg="Promisee/Compule Dispenser Gun";
    }
    else if(type=='Drawer Type Cotton Roll Dispense'){
      this.promiseeImg="Promisee/Cotton roll dispenser";
    }
    else if(type=='Denture Box'){
      this.promiseeImg="Promisee/Denture Box";
    }
    else if(type=='Dispenser Gun'){
      this.promiseeImg="Promisee/Dispenser-Gun-ConvertImage";
    }
    else if(type=='Disposable Impression Trays'){
      this.promiseeImg="Promisee/Disposable-Autoclavable-Dental-Plastic-Impression-Trays-for";
    }
    else if(type=='Disposable Tray'){
      this.promiseeImg="Promisee/monoart_einmaltrays_131439_1";
    }
    else if(type=='Face Mask'){
      this.promiseeImg="Promisee/Ruier-Dental-Protect-Face-Guard-with-10pcs";
    }
    else if(type=='Protective Shields'){
      this.promiseeImg="Promisee/Face shield";
    }
    else if(type=='Glass Dappen Dish'){
      this.promiseeImg="Promisee/Glass Dappen Dish";
    }
    else if(type=='Micro Applicator Brush'){
      this.promiseeImg="Promisee/SQ6B0027";
    }
    else if(type=='HP Mixing Tip'){
      this.promiseeImg="Promisee/Mixing Tips";
    }
    else if(type=='Mouth Prop & Tongue Guard'){
      this.promiseeImg="Promisee/SQ6B0034";
    }
    else if(type=='Mouth Prop'){
      this.promiseeImg="Promisee/SQ6B0035";
    }
    else if(type=='PLS-B009'){
      this.promiseeImg="Promisee/PLS-B009";
    }
    else if(type=='Spatula'){
      this.promiseeImg="Promisee/spatula";
    }
    else if(type=='Surgical Aspirator Tips'){
      this.promiseeImg="Promisee/SQ6B0025ed";
    }
    else if(type=='Wooden Wedges'){
      this.promiseeImg="Promisee/Wooden wedges";
    }
  }
  onBack(){
    this.router.navigate(['/Products/Disposables'])
  }
}
