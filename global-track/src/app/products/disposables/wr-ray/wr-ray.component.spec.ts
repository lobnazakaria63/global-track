import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrRayComponent } from './wr-ray.component';

describe('WrRayComponent', () => {
  let component: WrRayComponent;
  let fixture: ComponentFixture<WrRayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrRayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrRayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
