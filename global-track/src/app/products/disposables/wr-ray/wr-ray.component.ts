import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-wr-ray',
  templateUrl: './wr-ray.component.html',
  styleUrls: ['./wr-ray.component.css']
})
export class WrRayComponent implements OnInit {
  productTitle:string;
  raysonImg:string;
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type']; 
    this.productTitle=type;
    if(type=='Articulating Paper'){
      this.raysonImg="W.R.Rayson/W.R";
    }
  }
  onBack(){
    this.router.navigate(['/Products/Disposables'])
  }
}
