import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandPiecesHandInstrumentsComponent } from './hand-pieces-hand-instruments.component';

describe('HandPiecesHandInstrumentsComponent', () => {
  let component: HandPiecesHandInstrumentsComponent;
  let fixture: ComponentFixture<HandPiecesHandInstrumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandPiecesHandInstrumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandPiecesHandInstrumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
