import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Products} from "../../../models/products";
@Component({
  selector: 'app-hand-pieces-hand-instruments',
  templateUrl: './hand-pieces-hand-instruments.component.html',
  styleUrls: ['./hand-pieces-hand-instruments.component.css']
})
export class HandPiecesHandInstrumentsComponent implements OnInit {
  products:Products[]=[
    {
      image:"SOCO/3 way spray max push hand piece 4hole",
      key:"pSOCO",
      name:'G03Set Three way spray handpiece',
      company:"SOCO"
    },
    {
      image:"SOCO/45o  led push button with generator",
      key:"pSOCO",
      name:'45°handpiece',
      company:"SOCO"
    },
    {
      image:"SOCO/anti retraction high speed max push hand piece",
      key:"pSOCO",
      name:'G18Set Anti-retraction handpiece',
      company:"SOCO"
    },
    {
      image:"SOCO/External Low Speed Handpiece 4 Hole",
      key:"pSOCO",
      name:'External low speed kit',
      company:"SOCO"
    },
    {
      image:"SOCO/Inner Channel Low speed Contra Angle & Straight Handpiece 4 Hole",
      key:"pSOCO",
      name:'Inner channel low speed kit',
      company:"SOCO"
    },
    {
      image:"SOCO/interproximal enamel kit",
      key:"pSOCO",
      name:'interproximal enamel kit',
      company:"SOCO"
    },
    {
      image:"SOCO/LED Optic push High Speed Handpiece KAVO Fit 360 degree Water Adjustor",
      key:"pSOCO",
      name:'F19Set Optical quick handpiece KAVO',
      company:"SOCO"
    },
    {
      image:"SOCO/LED Optic push High Speed Handpiece Sirona Fit 360 degree Water Adjustor",
      key:"pSOCO",
      name:'F19Set Optical quick handpiece Sirona',
      company:"SOCO"
    },
    {
      image:"SOCO/LED Optic push NSK Fit High Speed Handpiece without  coupling",
      key:"pSOCO",
      name:'F19Set Optical quick handpiece NSK',
      company:"SOCO"
    },
    {
      image:"SOCO/Max led h.p with generator 4hole",
      key:"pSOCO",
      name:'Max led h.p with generator 4hole',
      company:"SOCO"
    },
    {
      image:"SOCO/Push Button Contra Angle for Implant External Spray",
      key:"pSOCO",
      name:'20：1Reduction contra angle',
      company:"SOCO"
    },
    {
      image:"SOCO/standard led h.p with generator 4hole",
      key:"pSOCO",
      name:'standard led h.p with generator 4hole',
      company:"SOCO"
    },
  ];

  filter1: any={key:'SOCO'};
  filter2:any={key:'Zoll'};
  filterAll:any={key:'p'};
  filter=this.filterAll;
  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  filterAllFunc(){
    this.filter=this.filterAll;
  }
  filterOneFunc(){
    this.filter=this.filter1;
  }
  filterTwoFunc(){
    this.filter=this.filter2;
  }
  
}
