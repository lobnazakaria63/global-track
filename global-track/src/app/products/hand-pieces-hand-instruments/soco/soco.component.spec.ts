import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocoComponent } from './soco.component';

describe('SocoComponent', () => {
  let component: SocoComponent;
  let fixture: ComponentFixture<SocoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
