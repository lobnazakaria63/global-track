import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SocoDescription, SocoTechData, SocoFeature } from '../../../../models/socoDetails';

@Component({
  selector: 'app-soco',
  templateUrl: './soco.component.html',
  styleUrls: ['./soco.component.css']
})
export class SocoComponent implements OnInit {
  productTitle:string;
  socoDesc:SocoDescription;
  socoTech:SocoTechData[];
  socoFeature:SocoFeature[];
  socoImg:string;
/*........................3 Way Spray Max Push.............................................*/
s3WaySprayDesc:SocoDescription={
  text:"Dental Three Way Spray Max Push Button Handpiece 4 Hole"
};
s3WaySprayTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mpa"},
  {variable:"Rotation speed",value:"more than 300,000rpm"},
  {variable:"Churk type",value:"push bottom"},
  {variable:"Bur applicabie",value:"1.59mm-1.6mm*21mm-23mm"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"Three way spray"},
];
s3WaySprayFeat:SocoFeature[]=[
  {point:"Torque push cartridge,the German dynamic balance processing To make every piece of blades for the wind,operation is very stable,low noise,speed is fast and no vibration,longer more durable service life"},
  {point:"Three way spray,good cooling system"},
  {point:"Can be 135℃ sterilized"},
  {point:"Complete handle"},
  {point:"Perfect stable and reliable performance"},
  {point:"Unique appearance design and fluent outline"},
  {point:"With best quality cartridge, easier to clean"},
  {point:"Only for people who have dentistry medical qualification"},
];
/*...............................................................................*/
s45DegDesc:SocoDescription={
  text:"Dental 45 Degree Surgical E Generator push High Speed Handpiece"
};
s45DegTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mp"},
  {variable:"Rotation speed",value:"more than 300,000 rpm"},
  {variable:"Chuck type",value:"push button"},
  {variable:"Bur applicabie",value:"Φ1.59mm~Φ1.6mm×21mm~23mm(diameter×length)"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"One way spray"},
  {variable:"shaft roundness",value:"0.001mm"},
  {variable:"Dynamic pulse rate",value:"≤0.03mm"},
  {variable:"Spindle chucks power",value:"22N（After 3,500 times"},
];
s45DegFeat:SocoFeature[]=[
  {point:"Sufficient LED white light and long service life,superior brightness,Longevity 10000 hours"},
  {point:"Unique electric Ggenerator,just a little air can generate sufficient power"},
  {point:"45° Contra-angle special use for the block teeth operation"},
  {point:"Bearing are bought from the best bearing manufacturer in China"},
  {point:"Impeller has been dynamic balance by the Germany machine. Enhance the useful life of the handpiece"},
  {point:"Can be 135℃ Sterilized"},
];
/*...............................................................................*/
sAntiDesc:SocoDescription={
  text:"Dental Anti-retraction High Speed Max Push Button Handpiece Compatible With NSK Pana max-TU"
};
sAntiTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-.027Mpa"},
  {variable:"Rotation speed",value:"more than 360,000rpm/more than 300,000rpm"},
  {variable:"Churk type",value:"push bottom"},
  {variable:"Bur applicabie",value:"1.59mm-1.6mm*21mm-23mm"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"one way spray three air spray"},
];
sAntiFeat:SocoFeature[]=[
  {point:"Torque push cartridge,the German dynamic balance processing To make every piece of blades for the wind,operation is very stable,low noise,speed is fast and no vibration,longer more durable service life"},
  {point:"Single way spray,good cooling effect"},
  {point:"It adopts the special mechanism designed to automally prevent the entry of oral fluids and other impurity into the handpiece head.Therefore,it will be not easy to get cross infection"},
  {point:"NSK style Pana Max high speed handpiece"},
  {point:"Ceramic bearing,sound less wear less"},
  {point:"Unique appearance design and fluent outline"},
  {point:"With best quality cartridge, easier to clean"},
  {point:"Only for people who have dentistry medical qualification"},
];
/*...............................................................................*/
exLowDesc:SocoDescription={
  text:"Dental External Low Speed Handpiece 4 Hole"
};
exLowTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.3Mpa"},
  {variable:"Rotation speed",value:"about 20,000 rpm"},
  {variable:"Chuck type",value:"Wrench style"},
  {variable:"Bur applicabie",value:"2.35mm(ISO1797-1)"},
  {variable:"Noise",value:"Less than 70dB"},
  {variable:"Spray",value:"External Channel"},
  {variable:"Hole",value:"4 holes"},
];
exLowFeat:SocoFeature[]=[
  {point:"Handle rotary part, which we adopt NSK's construction and technology. It is much easy to release the burs. While rotating it's more Smooth and durable. It will be not damaged by fatigue test of rotating more than 500 times."},
  {point:"Shaft which is using the oil bearing."},
  {point:"Can be 135℃ sterilized"},
  {point:"No vibration"},
  {point:"Only for people who have dentistry medical qualification."},
  {point:"Unique appearance design and fluent outline"},
];
/*...............................................................................*/
inChanDesc:SocoDescription={
  text:"Dental Inner Channel Low speed Contra Angle & Straight Handpiece 4 Hole"
};
inChanTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.3Mpa"},
  {variable:"Rotation speed",value:"about 20,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"2.35mm"},
  {variable:"Noise",value:"less than 70dB"},
  {variable:"Spray",value:"Inner Channel"},
  
];
inChanFeat:SocoFeature[]=[
  {point:"Inner fiber ,360 degrees of freedom rotary interface,avoid swinging to lead to prevent the handpiece wire be tensioned,unencumbered function of light,free to transform,reduce fatigue"},
  {point:"Can be 135℃ sterilized"},
  {point:"with 2pcs high speed bearing inside the shaft, works smoothly."},
  {point:"Adopting the High-tech equipment which is imported from Germany"},
  {point:"Skillful manufacturer and precise process"},
  {point:"with the top level of international dental equipment"},
  {point:"Connector compliance with ISO 9168 international standards"},
];
/*...............................................................................*/
enmalDesc:SocoDescription={
  text:""
};
enmalTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.3Mpa"},
  {variable:"Rotation speed",value:"about 20,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"2.35mm"},
  {variable:"Noise",value:"less than 70dB"},
  {variable:"Spray",value:"Inner Channel"},
];
enmalFeat:SocoFeature[]=[
  {point:""}
];
/*...............................................................................*/
kavoDesc:SocoDescription={
  text:"LED Optic push High Speed Handpiece KAVO Fit 360 degree Water Adjusto"
};
kavoTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mpa(6 hole)"},
  {variable:"Rotation speed",value:"more than 300,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"Φ1.59mm~Φ1.6mm×21mm~23mm(diameter×length)"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"three way spray"},
  {variable:"shaft roundness",value:"0.001mm"},
  {variable:"Dynamic pulse rate",value:"≤0.03mm"},
  {variable:"Spindle chucks powe",value:"22N(After 3,500 times)"},
];
kavoFeat:SocoFeature[]=[
  {point:"Sufficient LED white light and long service life,superior brightness,Longevity 10000 hours."},
  {point:"Quick coupling is compatible with KaVo MULTIfelx LUX."},
  {point:"360° Water adjustor."},
  {point:"hree way sprays - best cooling and continuous visibility"},
  {point:"Bearing are bought from the best bearing manufacturer in China."},
  {point:"Impeller has been dynamic balance by the Germany machine. Enhance the useful life of the handpiece."},
  {point:"Can be 135℃ Sterilized."},
];
/*...............................................................................*/
sironaDesc:SocoDescription={
  text:"LED Optic push High Speed Handpiece Sirona Fit 360 degree Water Adjustor"
};
sironaTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mpa(6 hole)"},
  {variable:"Rotation speed",value:"more than 300,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"Φ1.59mm~Φ1.6mm×21mm~23mm(diameter×length)"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"three way spray"},
  {variable:"shaft roundness",value:"0.001mm"},
  {variable:"Dynamic pulse rate",value:"≤0.03mm"},
  {variable:"Spindle chucks powe",value:"22N(After 3,500 times)"},
];
sironaFeat:SocoFeature[]=[
  {point:"Sufficient LED white light and long service life,superior brightness,Longevity 10000 hours."},
  {point:"Quick coupling is compatible with Sirona R/F"},
  {point:"360° Water adjustor."},
  {point:"Three way sprays - best cooling and continuous visibility."},
  {point:"Bearing are bought from the best bearing manufacturer in China."},
  {point:"Impeller has been dynamic balance by the Germany machine. Enhance the useful life of the handpiece"},
  {point:"Can be 135℃ Sterilized"},
];
/*...............................................................................*/
nskDesc:SocoDescription={
  text:"Dental LED Optic push NSK Fit High Speed Handpiece with quick coupling"
};
nskTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mpa(6 hole)"},
  {variable:"Rotation speed",value:"more than 300,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"Φ1.59mm~Φ1.6mm×21mm~23mm(diameter×length)"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"three way spray"},
  {variable:"shaft roundness",value:"0.001mm"},
  {variable:"Dynamic pulse rate",value:"≤0.03mm"},
  {variable:"Spindle chucks powe",value:"22N(After 3,500 times)"},
];
nskFeat:SocoFeature[]=[
  {point:"Sufficient LED white light and long service life,superior brightness,Longevity 10000 hours."},
  {point:"Quick coupling is compatible with NSK Machilite/Phatelus."},
  {point:"Three way sprays - best cooling and continuous visibility."},
  {point:"Bearing are bought from the best bearing manufacturer in China."},
  {point:"Impeller has been dynamic balance by the Germany machine. Enhance the useful life of the handpiece"},
  {point:"Can be 135℃ Sterilized"},
];
/*...............................................................................*/
maxGenDesc:SocoDescription={
  text:"Dental Stan Push Button LED Handpiece With Generator"
};
maxGenTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mpa(4 hole)"},
  {variable:"Rotation speed",value:"more than 300,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"Φ1.59mm~Φ1.6mm×21mm~23mm(diameter×length)"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"three way spray"},
];
maxGenFeat:SocoFeature[]=[
  {point:"Standard push cartridge,the German dynamic balance processing To make every piece of blades for the wind,operation is very stable,low noise,speed is fast and no vibration,longer more durable service life"},
  {point:"Three way spray,high efficient cooling System"},
  {point:"Universal to global dental chair"},
  {point:"Self-illumination without Connecting Circuit."},
  {point:"Appearance perfect stable and reliable performance"},
  {point:"Professional LED light source,superior brightness,Longevity 10000 hours，effectively solve the problem of perspective on the clinical treatment is not clear"},
  {point:"Unique electric generator,just a little air can generate sufficient power"},
  {point:"Can be 135℃ Sterilized"},
  {point:"Good quality best price"},
  {point:"With best quality cartridge, easier to clean"},
];
/*...............................................................................*/
contraDesc:SocoDescription={
  text:"Dental 20:1 Push Button Contra Angle for Implant External Spra"
};
contraTech:SocoTechData[]=[
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Reduction",value:"20:1"},
  {variable:"Hole diameter",value:"for CA burs Ф2.35"},
  {variable:"Bur applicabie",value:"2.35mm"},
  {variable:"Noise",value:"less than 70dB"},
  {variable:"Max torque",value:"50Ncm"},
  {variable:"Max speed",value:"40,000rpm"},
];
contraFeat:SocoFeature[]=[
  {point:"Simple,Effctive,disassemble it without tools and can be cleaned thoroughly to avoiding cross infection."},
  {point:"Can be 135 degree centigrade sterilized"},
  {point:"For implant"},
  {point:"External water spray"},
  {point:"Wide Range of Gear Ratio"},
  {point:"Ergonomic Comfort Grip"},
  {point:"Available for various E-type motors in accordance with ISO standard"},
];
/*...............................................................................*/
standGenDesc:SocoDescription={
  text:"Dental Stan Push Button LED Handpiece With Generator"
};
standGenTech:SocoTechData[]=[
  {variable:"Air pressure",value:"0.25-0.27Mpa(4 hole)"},
  {variable:"Rotation speed",value:"more than 300,000 rpm"},
  {variable:"Chuck type",value:"push bottom"},
  {variable:"Bur applicabie",value:"Φ1.59mm~Φ1.6mm×21mm~23mm(diameter×length)"},
  {variable:"Noise",value:"less than 68dB"},
  {variable:"Spray",value:"three way spray"},
];
standGenFeat:SocoFeature[]=[
  {point:"Standard push cartridge,the German dynamic balance processing To make every piece of blades for the wind,operation is very stable,low noise,speed is fast and no vibration,longer more durable service life"},
  {point:"Three way spray,high efficient cooling System"},
  {point:"Universal to global dental chair"},
  {point:"Self-illumination without Connecting Circuit."},
  {point:"Appearance perfect stable and reliable performance"},
  {point:"Professional LED light source,superior brightness,Longevity 10000 hours，effectively solve the problem of perspective on the clinical treatment is not clear"},
  {point:"Unique electric generator,just a little air can generate sufficient power"},
  {point:"Can be 135℃ Sterilized"},
  {point:"Good quality best price"},
  {point:"With best quality cartridge, easier to clean"},
];
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type'];
    this.productTitle=type;
    if(type=='G03Set Three way spray handpiece'){
      this.socoImg="SOCO/3 way spray max push hand piece 4hole";
      this.socoDesc=this.s3WaySprayDesc;
      this.socoTech=this.s3WaySprayTech;
      this.socoFeature=this.s3WaySprayFeat;
     }
     else if(type=='45°handpiece'){
      this.socoImg="SOCO/45o  led push button with generator";
      this.socoDesc=this.s45DegDesc;
      this.socoTech=this.s45DegTech;
      this.socoFeature=this.s45DegFeat;
     }
     else if(type=='G18Set Anti-retraction handpiece'){
      this.socoImg="SOCO/anti retraction high speed max push hand piece";
      this.socoDesc=this.sAntiDesc;
      this.socoTech=this.sAntiTech;
      this.socoFeature=this.sAntiFeat;
    }
    else if(type=='External low speed kit'){
      this.socoImg="SOCO/External Low Speed Handpiece 4 Hole";
      this.socoDesc=this.exLowDesc;
      this.socoTech=this.exLowTech;
      this.socoFeature=this.exLowFeat;
    }
    else if(type=='Inner channel low speed kit'){
      this.socoImg="SOCO/Inner Channel Low speed Contra Angle & Straight Handpiece 4 Hole";
      this.socoDesc=this.inChanDesc;
      this.socoTech=this.inChanTech;
      this.socoFeature=this.inChanFeat;
    }
    else if(type=='interproximal enamel kit'){
      this.socoImg="SOCO/interproximal enamel kit";
      this.socoDesc=this.enmalDesc;
      this.socoTech=this.enmalTech;
      this.socoFeature=this.enmalFeat;
    }
    else if(type=='F19Set Optical quick handpiece KAVO'){
      this.socoImg="SOCO/LED Optic push High Speed Handpiece KAVO Fit 360 degree Water Adjustor";
      this.socoDesc=this.kavoDesc;
      this.socoTech=this.kavoTech;
      this.socoFeature=this.kavoFeat;
    }
    else if(type=='F19Set Optical quick handpiece Sirona'){
      this.socoImg="SOCO/LED Optic push High Speed Handpiece Sirona Fit 360 degree Water Adjustor";
      this.socoDesc=this.sironaDesc;
      this.socoTech=this.sironaTech;
      this.socoFeature=this.sironaFeat;
    }
    else if(type=='F19Set Optical quick handpiece NSK'){
      this.socoImg="SOCO/LED Optic push NSK Fit High Speed Handpiece without  coupling";
      this.socoDesc=this.nskDesc;
      this.socoTech=this.nskTech;
      this.socoFeature=this.nskFeat;
    }
    else if(type=='Max led h.p with generator 4hole'){
      this.socoImg="SOCO/Max led h.p with generator 4hole";
      this.socoDesc=this.maxGenDesc;
      this.socoTech=this.maxGenTech;
      this.socoFeature=this.maxGenFeat;
    }
    else if(type=='20：1Reduction contra angle'){
      this.socoImg="SOCO/ush Button Contra Angle for Implant External Spray";
      this.socoDesc=this.contraDesc;
      this.socoTech=this.contraTech;
      this.socoFeature=this.contraFeat;
    }
    else if(type=='standard led h.p with generator 4hole'){
      this.socoImg="SOCO/standard led h.p with generator 4hole";
      this.socoDesc=this.standGenDesc;
      this.socoTech=this.standGenTech;
      this.socoFeature=this.standGenFeat;
    }
  }
  onBack(){
    this.router.navigate(['/Products/HandPieces and HandInstruments'])
  }

}
