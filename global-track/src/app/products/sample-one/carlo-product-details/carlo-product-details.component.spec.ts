import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarloProductDetailsComponent } from './carlo-product-details.component';

describe('CarloProductDetailsComponent', () => {
  let component: CarloProductDetailsComponent;
  let fixture: ComponentFixture<CarloProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarloProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarloProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
