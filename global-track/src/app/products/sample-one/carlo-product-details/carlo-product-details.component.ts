import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CarloDescription, CarloTechDetails, CarloStandCompo } from '../../../../models/carloDetails';

@Component({
  selector: 'app-carlo-product-details',
  templateUrl: './carlo-product-details.component.html',
  styleUrls: ['./carlo-product-details.component.css']
})
export class CarloProductDetailsComponent implements OnInit {
  productTitle:string;
  carloDes:CarloDescription[];
  carloTech:CarloTechDetails[];
  carloStandComp:CarloStandCompo[];
  carloImg:string;
  isElectro=false;
/*.....................CARLO DE GIORGI AUTOCLAVE.................................*/
autocalveDescrip:CarloDescription[]=[
  {text:"Very simple to use, sure for the operator it a machine to sterilize handpieces, turbines, rotating instruments and each instrument sterilizable at 125 °C."},
  {text:"WS 01 can do all this very easy, simply by closing the cover and select the cycle lasting 15 minutes."},
  {text:"WS 01 is formed by an external part of painted steel, by a 18/10 stainless steel vapor pressure boiler without any welding and by a lid in stainless steel too with a removable basket."},
]
autoclaveTech:CarloTechDetails[]=[
  {variable:"External dimensions",value:"30x29x4 cm"},
  {variable:"Chamber dimensions",value:"22x23,5 cm"},
  {variable:"Basket",value:"21x21,5 cm"},
  {variable:"Sterilizing liquid",value:"distilled water"},
  {variable:"Rated Power",value:"230 V - 50/60 HZ"},
  {variable:"Exercise temperature",value:"125 °C"},
  {variable:"Sterilization time",value:"15 min"},
  {variable:"Exercise pressure",value:"1,6/1,7 bar"},
  {variable:"Weight",value:"12 Kg"}
];
/*......................................................*/
/*..........................CARLO DE GIORGI ELECTROSURGRY.................................*/
electroDescrip:CarloDescription[]=[
  {text:"Surgery plus is a radiofrequency electrosurgical unit suitable for dental monopolar surgery."},
  {text:"Surgery plus through the functions selection makes pure cut, cut coagulation (CUT 1 - CUT 2) and pure coagulation possible."},
  {text:"The on-board microcontroller and digital display guarantees consistent safety and reliability under any conditions."},
  {text:" Surgery plus offers outstanding professional surgical results due to its ergonomic and superior safety features."}
];
electroTech:CarloTechDetails[]=[
  {variable:'Maximum output power CUT',value:'50 W - 400 '},
  {variable:'Maximum output power CUT/COAG1',value:'45 W - 400 '},
  {variable:'Maximum output power CUT/COAG2',value:'40 W - 400 '},
  {variable:'Maximum output power COAG',value:'40 W - 400'},
  {variable:'Working frequency',value:'600 kHz'},
  {variable:'Patient circuit',value:'F'},
  {variable:'Selectable input voltage',value:'115/230 Vac'},
  {variable:'Mains frequency',value:'50/60 Hz'},
  {variable:'Electrical input powe',value:'280 VA'},
  {variable:'Size WxHxD',value:'190x85x239 mm'},
  {variable:'Weight',value:'2,5 Kg'},
]
electroStandComp:CarloStandCompo[]=[
  {point:'Reusable monopolar handle.'},
  {point:'Assorted electrodes (10 pcs).'},
  {point:'Cable for rod neutral electrode'},
  {point:'Rod neutral electrode.'},
  {point:'Foot switch.'},
  {point:'Power supply cable.'},
]
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type'];
    this.productTitle=type;
    if(type=='Carlo de giorgi Autoclave'){
      this.carloImg="Carlo de giorgi/Carlo de giorgi Autoclave";
      this.carloDes=this.autocalveDescrip;
      this.carloTech=this.autoclaveTech;
     }
    else if(type=='carlo de giorgi electrosurgery'){
      this.carloImg="Carlo de giorgi/carlo de giorgi electrosurgery";
      this.carloDes=this.electroDescrip;
      this.carloTech=this.electroTech;
      this.carloStandComp=this.electroStandComp;
      this.isElectro=true;
    }
  }
  onBack(){
    this.router.navigate(['Products/Equipments'])
  }
}
