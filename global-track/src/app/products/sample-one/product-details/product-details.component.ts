import { Component, OnInit } from '@angular/core';
import { ProductDetails, ProductCharacter, TechParam } from '../../../../models/sunProductDetails';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  productTitle:string;

  //sun products properties..............................
  sunUltrasonicImg="SUN/SUN ultrasonic cleaner";
  sunStainlessImg="SUN/stainless steel sealing machine";
  sunPlasticImg="SUN/plastic sealing machine";
  sunAutoImg="SUN/SUN Autoclave";
  sun_iii="SUN/sun-lll"
  sunImg:string;
 sunProductdet:ProductDetails[];
 sunProChara:ProductCharacter[];
 sunTechPara:TechParam[];
 proDetHeaders:string[]=[];
 isAuto=false;
 //................................................


//sun product Arrays...........................

/*......SUN ULTRASONIC PRODUCTS DETAILS..........*/
sunUltrasonicProducts:ProductDetails[]=[{
  result1:'SUN-U505',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±1Hz',
  result4:'120W',
  result5:'5Litres',
  result6:'240×140×150mm',
  result7:'330×210×270mm',
  result8:'360×260×340mm',
  result9:'5.4kg',
}]
/*......................SUN ULTRASONIC PRODUCT CHARACTER.............................*/
sunUltrasonicProChara:ProductCharacter[]=[
  {text:"Time of cleaning can be continued measured(I-60 minutes)."},
  {text:"Temperature of cleaning can be continued(20-80centigrade) and itwill cutoff the power automatically when it is lack of water."},
  {text:'The outside of it is all made of stainless steel,so it is beautiful and decent.'},
  {text:' For its design of low noisy so it can keep the clinic quiet.'},
  {text:'Special apparatus basket is suitable for the apparatus which is delicate.'},
  {text:'fine and can afford any friction.such as high or low speed combined set.'}
]
//............................................................
/*......SUN STAINLESS PRODUCTS DETAILS..........*/
sunStainlessProducts:ProductDetails[]=[{
  result1:'SUN-S730',
  result2:'AC220V±10%',
  result3:'50/60Hz±1Hz',
  result4:'500W',
  result5:'T3A',
  result6:'250',
  result7:'10mm',
  result8:'430×350×260mm',
  result9:'7.5kg',
},
{
  result1:'SUN-S740',
  result2:'AC220V±10%',
  result3:'50/60Hz±1Hz',
  result4:'500W',
  result5:'T3A',
  result6:'250',
  result7:'10mm',
  result8:'430×350×260mm',
  result9:'7.5kg',
}]
/*......................SUN STAINLESS PRODUCT CHARACTER.............................*/
sunStainlessProChara:ProductCharacter[]=[
  {text:"High auality stainless steel fashionable body."},
  {text:"With extra blad，easy to replace."},
  {text:'Attractive plastic body'},
  {text:'Fashion design，with safety insulation'},
  {text:'Easy to replace the blade'},
  {text:'External standard surgical knife blade'},
  {text:'It consists of medical paper and polypropylene layer,The change of font color indicates the finishing of the sterilizing process.'},
  {text:'Put the instruments which need sterilized in sealed package and save them for a long time.'},
  {text:'The product has different width specifications of 5cm，7cm and 10cm to meet the seal requirements of instruments with different dimensions.'}
]
/*....................................................*/
/*......SUN PLASTIC PRODUCTS DETAILS..........*/
sunPlasticProducts:ProductDetails[]=[{
  result1:'SUN-S730',
  result2:'AC220V±10%',
  result3:'50/60Hz±1Hz',
  result4:'500W',
  result5:'T3A',
  result6:'250',
  result7:'10mm',
  result8:'430×350×260mm',
  result9:'7.5kg',
},
{
  result1:'SUN-S740',
  result2:'AC220V±10%',
  result3:'50/60Hz±1Hz',
  result4:'500W',
  result5:'T3A',
  result6:'250',
  result7:'10mm',
  result8:'430×350×260mm',
  result9:'7.5kg',
}]
/*......................SUN PLASTIC PRODUCT CHARACTER.............................*/
sunPlasticProChara:ProductCharacter[]=[
  {text:"High auality stainless steel fashionable body."},
  {text:"With extra blad，easy to replace."},
  {text:'Attractive plastic body'},
  {text:'Fashion design，with safety insulation'},
  {text:'Easy to replace the blade'},
  {text:'External standard surgical knife blade'},
  {text:'It consists of medical paper and polypropylene layer,The change of font color indicates the finishing of the sterilizing process.'},
  {text:'Put the instruments which need sterilized in sealed package and save them for a long time.'},
  {text:'The product has different width specifications of 5cm，7cm and 10cm to meet the seal requirements of instruments with different dimensions.'}

]
/*....................................................*/
/*......SUN PLASTIC PRODUCTS DETAILS..........*/
sunAutoclaveProducts:ProductDetails[]=[{
  result1:'SUN23-II',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±1Hz',
  result4:'2000W',
  result5:'23Litres',
  result6:'445X640X395mm',
  result7:'Ø249x450mm',
  result8:'720×520×500mm',
  result9:'53kg',
},
{
  result1:'SUN18-II',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±lHz',
  result4:'1800W',
  result5:'18Litres',
 result6:'445X550X395mm',
  result7:'Ø249x355mm',
  result8:'670×550×500mm',
  result9:'48kg',
},
{
  result1:'SUN16-II',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±1Hz',
  result4:'1800W',
  result5:'16Litres',
  result6:'445X550X395mm',
  result7:'Ø230x360mm',
  result8:'740×550×500mm',
  result9:'47kg',
},
{
  result1:'SUN12-II',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±1Hz',
  result4:'1800W',
  result5:'12Litres',
  result6:'445X550X395mm',
  result7:'Ø200x360mm',
  result8:'670×550×500mm',
  result9:'46kg',
}]
/*......................SUN UAUTOCLAVE PRODUCT CHARACTER.............................*/
sunAutoclaveProChara:ProductCharacter[]=[
  {text:" Based on the new class B of international prEN13060, have 3 pulsatingvacuum, Vacuum up to-0.9Bar (-0.09Mpa).Suitable to sterilizing wrap,unwrap, solid, porous, hollow device."},
  {text:"Use imported&advanced 16-bit microprocessor,easy to operate. Most suitable for the Dept of Stomatology,Ophthalmology, Surgery and Lab."},
  {text:'.With B&D test and vacuum test procedures. To test the penetrability.'},
  {text:'The jet type of steam generator,ensure the efficient sterilizing.'},
  {text:'With alarm system for waste water tank, avoid the waste water to enter into the sterilizing circulation, make the sterilizing completely.'},
  {text:'Precise digital display and advanced self-test system ensure you get the working data freely.'},
  {text:'Double lock door system for safety during operating.'},
  {text:'Optional inbuilt mini一printer or USB connector to record the process of sterilizing.'},
  
]
/*......................SUN UAUTOCLAVE TECHNICAL PARAMETER.............................*/
sunAutoTechPara:TechParam[]=[
    {variable:"Sterilization level", value:"European Class B standard"},
    {variable:"Sterilizing temperature", value:"134℃/121℃"},
    {variable:"Special sterilization", value:"Kill active HV,HBV,BSE and spore"},
    {variable:"Dry procedure", value:"Strong vacuum drying,the residual humidity<0.2%"},
    {variable:"Display", value:"Widescreen LCD"},
    {variable:"Test", value:"Bowie&Dick Steam penetration test,  vacuum test , Helix test"},
    {variable:"Safety", value:"Safety valve, Doble Pressure lock Automatic check."},
    {variable:"Sterilizing record", value:"Optional USB and printer (internal or external) to record the sterilization process"},
    {variable:"Water supply system", value:"3L distiller water tank,3L waste water tank"},
    {variable:"Clean procedure", value:"Automatic clean inside pipe and steam generator"},
    {variable:"Instrument containers", value:"5 levels with 3 Round trays"}
]
//........................................................
/*......SUN III PRODUCTS DETAILS..........*/
suniiiDetail:ProductDetails[]=[{
  result1:'SUN23-III-X',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±1Hz',
  result4:'2000W',
  result5:'23Litres',
  result6:'475X610X410mm',
  result7:'Ø249x450mm',
  result8:'750×595×525mm',
  result9:'53kg',
},
{
  result1:'SUN18-III-X',
  result2:'AC220V/110V±10%',
  result3:'50/60Hz±lHz',
  result4:'1800W',
  result5:'18Litres',
  result6:'475X530X410mm',
  result7:'Ø249x355mm',
  result8:'675×595×525mm',
  result9:'50kg',
}]
/*......................SUN ULTRASONIC PRODUCT CHARACTER.............................*/
suniiiCharacter:ProductCharacter[]=[
  {text:"Based on the new class B of international prEN13060, have 3 pulsating vacuum, Vacuum up to -0.9Bar (-0.09Mpa).Suitable to sterilizing wrap, unwrap, solid, porous, hollow device."},
  {text:"Use imported&advancedl6-bit microprocessor,easy to operate. Most suitable for the Dept of Stomatology,Ophthalmology, Surgery and Lab."},
  {text:'.With B& D test and vacuum test procedures. To test the penetrability.'},
  {text:'Chamber punched by Stainless Steel (#304, thickness: 2.5mm), together with The jet type of steam generator,self-cleaning ensure the efficient sterilizing.'},
  {text:'.With alarm system for waste water tank, avoid the waste water to enter into the sterilizing circulation, make the sterilizing completely.'},
  {text:'.Precise LED digital display and advanced self-test system ensure you get the working data freely.'},
  {text:'Double lock door system for safety during operating.'},
  {text:'with built-in printer .USB optional. The disinfection process data and date automatically print.'}
]
//............................................................
/*......................SUN UAUTOCLAVE TECHNICAL PARAMETER.............................*/
suniiiTechPara:TechParam[]=[
  {variable:"Sterilization level", value:"European Class B standard"},
  {variable:"Sterilizing temperature", value:"134℃/121℃"},
  {variable:"Special sterilization", value:"Kill active HV,HBV,BSE and spore"},
  {variable:"Dry procedure", value:"Strong vacuum drying,the residual humidity<0.2%"},
  {variable:"Display", value:"Widescreen LCD"},
  {variable:"Test", value:"Bowie&Dick Steam penetration test,  vacuum test , Helix test"},
  {variable:"Safety", value:"Safety valve, Doble Pressure lock Automatic check."},
  {variable:"Sterilizing record", value:"Optional USB and printer (internal or external) to record the sterilization process"},
  {variable:"Water supply system", value:"3L distiller water tank,3L waste water tank"},
  {variable:"Clean procedure", value:"Automatic clean inside pipe and steam generator"},
  {variable:"Instrument containers", value:"5 levels with 3 Round trays"}
]
//........................................................
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type'];
    this.productTitle=type;
    if(type=='plastic sealing machine'){
      this.productTitle='plastic Sealing Machine';
      this.sunImg=this.sunPlasticImg;
      this.sunProductdet=this.sunPlasticProducts;
      this.sunProChara=this.sunPlasticProChara;
      this.proDetHeaders=["Model","Voltage","Frequency","Power","Fuse Tube","MM (Max)","Embossing Width","Outside Dimension",'G"W'];
    }
    else if(type=='stainless steel sealing machine'){
      this.productTitle='stainless steel sealing machine';
      this.sunImg=this.sunStainlessImg;
      this.sunProductdet=this.sunStainlessProducts;
      this.sunProChara=this.sunStainlessProChara;
      this.proDetHeaders=["Model","Voltage","Frequency","Power","Fuse Tube","MM (Max)","Embossing Width","Outside Dimension",'G"W'];
    }
    else if(type=='SUN ultrasonic cleaner'){
      this.productTitle='SUN ultrasonic cleaner';
      this.sunImg=this.sunUltrasonicImg;
      this.sunProductdet=this.sunUltrasonicProducts;
      this.sunProChara=this.sunUltrasonicProChara;
      this.proDetHeaders=["Model","Voltage","Frequency","Power","Tank Capacity","Appearance size","Inside Dimension","Outside Dimension",'G"W'];
    }
    else if(type=='SUN Autoclave'){
      this.isAuto=true;
      this.productTitle='SUN Autoclave';
      this.sunImg=this.sunAutoImg;
      this.sunProductdet=this.sunAutoclaveProducts;
      this.sunProChara=this.sunAutoclaveProChara;
      this.sunTechPara=this.sunAutoTechPara;
      this.proDetHeaders=["Model","Voltage","Frequency","Power","Tank Capacity","Appearance size","Inside Dimension","Outside Dimension",'G"W'];
      
    }
    else if(type=='SUN-III-X'){
      this.isAuto=true;
      this.productTitle='SUN-III-X';
      this.sunImg=this.sun_iii;
      this.sunProductdet=this.suniiiDetail;
      this.sunProChara=this.suniiiCharacter;
      this.sunTechPara=this.suniiiTechPara;
      this.proDetHeaders=["Model","Voltage","Frequency","Power","Tank Capacity","Appearance size","Inside Dimension","Outside Dimension",'G"W'];
      
    }
   
    
  }
onBack(){
  this.router.navigate(['Products/Equipments'])
}
}
