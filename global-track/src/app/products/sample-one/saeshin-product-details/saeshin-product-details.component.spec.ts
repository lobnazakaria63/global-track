import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaeshinProductDetailsComponent } from './saeshin-product-details.component';

describe('SaeshinProductDetailsComponent', () => {
  let component: SaeshinProductDetailsComponent;
  let fixture: ComponentFixture<SaeshinProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaeshinProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaeshinProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
