import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SaeshinProDetails, SaeshinProFeature, SaeshinCompHand, SaeshinController } from '../../../../models/saeshinProductDetails';

@Component({
  selector: 'app-saeshin-product-details',
  templateUrl: './saeshin-product-details.component.html',
  styleUrls: ['./saeshin-product-details.component.css']
})
export class SaeshinProductDetailsComponent implements OnInit {
  productTitle:string;
  saeshinProDetails:SaeshinProDetails[];
  saeshinProFeature:SaeshinProFeature[];
  saeshinCompHand:SaeshinCompHand[];
  saeshinCotroller:SaeshinController[];
  saeshinImg:string;
  asideName:string;
  handPiece:string;
  notCube:boolean;
  /*.................FORTE 200a ( LABORATORT MICROMOTOR 2)......................*/
  forte2Details:SaeshinProDetails[]=[
    {text:"Knee Control Type"},
    {text:"Ergonomically Designed"},
    {text:"Exellent Durability"}
  ]
  forte2Feature:SaeshinProFeature[]=[
    {header:"Low voice and vibration, Maximal efficiency",text:""},
    {header:"Hand/Feet speed control system",text:""},
    {header:"Built-in micro processor",text:'Self-diagnosis with "error" display, usage memory storage and handpiece auto-recognistion function.'},
    {header:"Foot control speed is optional",text:""}
  ]
  forte2CompHand:SaeshinCompHand[]=[
    {variable:"RPM",value:"MAX 50,000 RPM"},
    {variable:"Torque",value:"7.3 N.cm"},
    {variable:"Dimension (mm)",value:"L 171 X L1 81 X L2 90 X D 29 X D1 19"},
    {variable:"Weight",value:"254g (Without Cord)"},
  ]
  forte2Controller:SaeshinController[]=[
    {variable:"RPM",value:"MAX 50,000 RPM"},
    {variable:"Input",value:"AC 100-120 V~/220-240V~(50/60 Hz)"},
    {variable:"Output",value:"230W"},
    {variable:"Weight",value:"3K"},
  ]
  /*.................OZ BLACK ( LABORATORT MICROMOTOR 3)......................*/
  ozDetails:SaeshinProDetails[]=[
    {text:"Desktop Type"},
    {text:"Ergonomically Designed"},
    {text:"Exellent Durability"}
  ]
  ozFeature:SaeshinProFeature[]=[
    {header:"Overload Display",text:""},
    {header:"Low voice and vibration, Maximal efficiency",text:""},
    {header:"Hand/Feet speed control system",text:""},
    {header:"Built-in micro processor",text:'Self-diagnosis with "error" display, usage memory storage and handpiece auto-recognistion function.'}
    
  ]
  ozCompHand:SaeshinCompHand[]=[
    {variable:"RPM",value:"MAX 50,000 RPM"},
    {variable:"Torque",value:"7.8 N.cm"},
    {variable:"Dimension (mm)",value:"L 171 X L1 81 X L2 90 X D 29 X D1 19"},
    {variable:"Weight",value:"254g (Without Cord)"},
  ]
  ozController:SaeshinController[]=[
    {variable:"RPM",value:"MAX 50,000 RPM"},
    {variable:"Input",value:"AC 100-120 V~/220-240V~(50/60 Hz)"},
    {variable:"Output",value:"230W"},
    {variable:"Weight",value:"2.5K"},
  ]
  /*.................FORTE 100a ( LABORATORT MICROMOTOR)......................*/
  forte1Details:SaeshinProDetails[]=[
    {text:"Desktop Type"},
    {text:"Ergonomically Designed"},
    {text:"Exellent Durability"}
  ]
  forte1Feature:SaeshinProFeature[]=[
    {header:"Low voice and vibration, Maximal efficiency",text:""},
    {header:"Hand/Feet speed control system",text:""},
    {header:"Built-in micro processor",text:'Self-diagnosis with "error" display, usage memory storage and handpiece auto-recognistion function.'}
    
  ]
  forte1CompHand:SaeshinCompHand[]=[
    {variable:"RPM",value:"MAX 50,000 RPM"},
    {variable:"Torque",value:"4.2 N.cm"},
    {variable:"Dimension (mm)",value:"L 171 X L1 81 X L2 90 X D 29 X D1 19"},
    {variable:"Weight",value:"254g (Without Cord)"},
  ]
  forte1Controller:SaeshinController[]=[
    {variable:"RPM",value:"MAX 50,000 RPM"},
    {variable:"Input",value:"AC 100-120 V~/220-240V~(50/60 Hz)"},
    {variable:"Output",value:"230W"},
    {variable:"Weight",value:"2.9K"},
  ]
  /*.................X-CUBE IMPANT MOTOR......................*/
  cubeDetails:SaeshinProDetails[]=[
    {text:"Our Most Popular Implant Engine"},
    {text:"Economically priced moderl with korean high technology"},
    {text:"Provides optimized performance with a BLCD motor (0-50,000rpm) and contra angle handpiece"}
  ]
  cubeFeature:SaeshinProFeature[]=[
    {header:"Programe Memory Function",text:"Maximum 9 programmable memories for setting Speed, Torque, Rotation, Direction, Irrigation Pump, Intensity and Boost"},
    {header:"Automatic Overload Protection Function",text:" The Motor is Automatically stopped and 'error' sign display on screen when the load is higher than Torque value"},
    {header:"Realtime RPM, TORQUE view system",text:"Actual RPM and Torque value are displayed on the screen, it makes user can do more delicate operation "},
    {header:"Motor Auto-Calibration Function",text:"This Model is manufactured by ISO which can be compatible with products of advanced companies like NSK, KAVO etc.."}
  ]
 
  cubeController:SaeshinController[]=[
    {variable:"Input",value:"AC 100-120 V,220-240 V"},
    {variable:"Fuse",value:"5A / 2A"},
    {variable:"Frequency",value:"50/60 Hz"},
    {variable:"Output",value:"230W"},
    {variable:"Maximum Pump",value:"Max. 75ml/min"},
    {variable:"Dimension (mm)",value:"W205 x D210 xH136"},
    {variable:"weight",value:"3.3kg"},
  ]
  /*.................Strong 204 Micromotor + Contra MICROMOTOR......................*/
  strongDetails:SaeshinProDetails[]=[
    {text:"Basic Model"},
    {text:"Mini Compact Design"},
    {text:"Less-Vibration Standared Carbon Brush Motor"}
  ]
  strongFeature:SaeshinProFeature[]=[
    {header:"Right And Left Turning Ability",text:""},
    {header:"Non Stage Speed System",text:""},
    {header:"Foot On/Off Control",text:""},
    
  ]
  strongController:SaeshinController[]=[
    {variable:"RPM",value:"MAX 35,000 RPM"},
    {variable:"Torque",value:"280 gf.cm"},
    {variable:"Input",value:"100V ,110V ,230V ,240V (50/60Hz) "},
    {variable:"Output",value:"DC0-32V"},
    {variable:"Weight",value:"Control box 1.7kg"},
    {variable:"HandPiece",value:"200g (STRONG 102L)"},
  ]
  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    let type=this.route.snapshot.params['type'];
    this.productTitle=type;
    if(type=='Saeshin laboratory micromotor 2'){
        this.saeshinImg="Saeshin/Saeshin laboratory micromotor 2";
        this.asideName="FORTE 200a "
        this.handPiece="F100alll"
        this.saeshinProDetails=this.forte2Details;
        this.saeshinProFeature=this.forte2Feature;
        this.saeshinCotroller=this.forte2Controller;
        this.saeshinCompHand=this.forte2CompHand;
        this.notCube=true;
      }
      else if(type=='saeshin laboratory micromotor 3'){
        this.saeshinImg="Saeshin/saeshin laboratory micromotor 3";
        this.asideName="OZ Black "
        this.handPiece="F100alll"
        this.saeshinProDetails=this.ozDetails;
        this.saeshinProFeature=this.ozFeature;
        this.saeshinCotroller=this.ozController;
        this.saeshinCompHand=this.ozCompHand;
        this.notCube=true;
      }
      else if(type=='Saeshin Laboratory Micromotor'){
        this.saeshinImg="Saeshin/Saeshin Laboratory Micromotor";
        this.asideName="FORTE 100a"
        this.handPiece="F100alll"
        this.saeshinProDetails=this.forte1Details;
        this.saeshinProFeature=this.forte1Feature;
        this.saeshinCotroller=this.forte1Controller;
        this.saeshinCompHand=this.forte1CompHand;
        this.notCube=true;
      }
      else if(type=='X-cube impant motor'){
        this.saeshinImg="Saeshin/X-cube impant motor";
        this.saeshinProDetails=this.cubeDetails;
        this.saeshinProFeature=this.cubeFeature;
        this.saeshinCotroller=this.cubeController;
        this.notCube=false;
      }
     
      else if(type=='Strong 204 Micromotor + Contra'){
        this.saeshinImg="Saeshin/strong 204/SQ6B9976";
        this.saeshinProDetails=this.strongDetails;
        this.saeshinProFeature=this.strongFeature;
        this.saeshinCotroller=this.strongController;
      }
  }
  onBack(){
    this.router.navigate(['Products/Equipments'])
  }
}
