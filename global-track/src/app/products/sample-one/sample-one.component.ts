import { Component, OnInit } from '@angular/core';
import * as mixer from  "mixitup";
import * as $ from 'jquery';
import {Products} from "../../../models/products";
@Component({
  selector: 'app-sample-one',
  templateUrl: './sample-one.component.html',
  styleUrls: ['./sample-one.component.css']
})
export class SampleOneComponent implements OnInit {
  products:Products[]=[
    {
      image:"SUN/plastic sealing machine",
      key:"pSun",
      name:'plastic sealing machine',
      company:"SUN"
    },
    {
      image:"SUN/stainless steel sealing machine",
      key:"pSun",
      name:"stainless steel sealing machine",
      company:"SUN"
    },
    {
      image:"SUN/SUN Autoclave",
      key:"pSun",
      name:"SUN Autoclave",
      company:"SUN"
    },
    {
      image:"SUN/SUN ultrasonic cleaner",
      key:"pSun",
      name:"SUN ultrasonic cleaner",
      company:"SUN"
    },
    {
      image:"SUN/sun-lll",
      key:"pSun",
      name:"SUN-III-X",
      company:"SUN"
    },
    {
      image:"Carlo de giorgi/Carlo de giorgi Autoclave",
      key:"pCarlo",
      name:"Carlo de giorgi Autoclave",
      company:"Carlo de giorgi"
    },
    {
      image:"Carlo de giorgi/carlo de giorgi electrosurgery",
      key:"pCarlo de giorgi",
      name:"carlo de giorgi electrosurgery",
      company:"Carlo de giorgi"
    },
    {
      image:"Saeshin/Saeshin laboratory micromotor 2",
      key:"pSaeshin",
      name:"Saeshin laboratory micromotor 2",
      company:"Saeshin"
    },
    {
      image:"Saeshin/saeshin laboratory micromotor 3",
      key:"pSaeshin",
      name:"saeshin laboratory micromotor 3",
      company:"Saeshin"
    },
    {
      image:"Saeshin/Saeshin Laboratory Micromotor",
      key:"pSaeshin",
      name:"Saeshin Laboratory Micromotor",
      company:"Saeshin"
    },
    
    {
      image:"Saeshin/strong 204/SQ6B9976",
      key:"pSaeshin",
      name:"Strong 204 Micromotor + Contra",
      company:"Saeshin"
    },
    {
      image:"Saeshin/X-cube impant motor",
      key:"pSaeshin",
      name:"X-cube impant motor",
      company:"Saeshin"
    },
   

  ]
  
  filter1: any={key:'Sun'};
  filter2:any={key:'Saeshin'};
  filter3:any={key:'Carlo'};
  filterAll:any={key:'p'};
  filter=this.filterAll;
  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
    
  }
  filterAllFunc(){
    this.filter=this.filterAll;
  }
  filterOneFunc(){
    this.filter=this.filter1;
  }
  filterTwoFunc(){
    this.filter=this.filter2;
  }
  filterThreeFunc(){
    this.filter=this.filter3;
  }

}
