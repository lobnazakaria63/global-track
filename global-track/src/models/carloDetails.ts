export interface CarloDescription{
    text:string;
}
export interface CarloTechDetails{
    variable:string;
    value:string;
}
export interface CarloStandCompo{
    point:string;
}