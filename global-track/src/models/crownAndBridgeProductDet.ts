export interface ProductDesc{
    text:string;
}
export interface PrimeProductBenefitAndFeature{
    point:string;
}

export interface PrimeProductTechData{
    variable:string;
    value:string;
}
export interface RisteaProductInfo{
    res1:string;
    res2:string;
    res3:string;
}