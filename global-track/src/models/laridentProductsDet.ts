export interface CodeDesc{
    variable:string;
    value:string;
}
export interface Description{
    text:string;
}