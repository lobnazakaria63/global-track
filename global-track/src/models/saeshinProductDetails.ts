export interface SaeshinProDetails{
text:string;
}
export interface SaeshinProFeature{
    header:string;
    text:string;
}
export interface SaeshinCompHand{
    variable:string;
    value:string;
}
export interface SaeshinController{
    variable: string;
    value:string;
}