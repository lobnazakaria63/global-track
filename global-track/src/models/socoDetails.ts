export interface SocoDescription{
    text:string;
}
export interface SocoTechData{
    variable:string;
    value:string;
}
export interface SocoFeature{
    point:string;
}