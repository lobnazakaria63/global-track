export interface ProductDetails {
     result1:string;
     result2:string;
     result3:string;
     result4:string;
     result5:string;
     result6:string;
     result7:string;
     result8:string;
     result9:string;
}
export interface ProductCharacter{
    text:string;
}
export interface TechParam{
    variable:string;
    value:string;
}